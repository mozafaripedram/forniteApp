﻿Levelling up is part of the gameplay loop in Fortnite, but is there a way to do it faster?

We're at the begnning of a new season so everyone and their grandma is wondering how to level up fast in Fortnite. Getting to the end of the new battle pass and unlocking all the new cosmetics in Fortnite is a noble goal, but quite the challenge if you don't have time to play all day, every day. There are plenty of ways to maximise your XP gains though, so we're here to help with this guide on how to level up quickly in Fortnite.

Every single day, you’re given a new challenge, and you can have up to a maximum of three at any one time. They can range from getting catching items while fishing to searching chests in a specific location and so much more, and completing each one rewards you with a hefty amount of XP each time. Even if you can’t complete the challenge that day, make sure you at least log on to collect it or check out which ones you've been dealt that day. Fortnite is now available on iOS, Nintendo Switch and Android so there’s almost no reason you can’t just open the game once per day to collect the challenge then complete it at a later date.

Don’t forget that you can also re-roll one challenge per day if you’re not fond of what it’s offering. Need to place top 50 in solo three times but you only play with pals? Re-roll it and there’s a good chance you’ll be granted an objective that is more suitable to your style of play. Unfortunately, you can’t re-roll the weekly challenges, so you need to keep attempting those until you’re successful.

-Tick off those challenges and earn rewards with the Battle Pass
Unlike previous seasons where you had to buy the battle pass to complete all of the missions and weekly challenges, forking out money isn't a requirement anymore. There's new missions every week and you'll earn a whopping 52,000 XP per challenge. That means you can earn 572,000 XP every week which will have you flying through the battle pass in no time at all.
If you're struggling with any of the Fortnite Missions, our handy guide has walkthroughs for all of the tough tasks from each week. Before the season has concluded in February, work your way through everything you've missed and earn all the rewards you can.

-Play with friends and use Party Assist to help complete challenges quicker
If there is a specific challenge you're really struggling to complete, enlist the help of some friends. The Party Assist function means you can get your squad mates to help you out with a specific challenge, so when they complete it, you'll also get it done. For example, if you're unable to get enough kills or find enough supply drops, turn this on and your entire party's progression will count towards it.

-Earn medals for smaller XP boosts
Introduced at the start of Chapter 2, medals are small gameplay achievements you can earn while playing. There's a plethora of tasks you can complete to start decking out your punchcard, from surviving longer in the match to earning kills and searching chests.
Your punchcard will reset every day, so as long as you keep playing every day, you can max it out and earn a tremendous amount of XP on top of all the daily and weekly challenges you complete. You'll reach tier 100 in no time!