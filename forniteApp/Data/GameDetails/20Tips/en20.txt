﻿All the best Fortnite tips for help with building, finding weapons, staying stealthy and very much alive

Having a set of Fortnite tips to hand can help you navigate the battle royale, especially when there are so many changes being introduced with each new season. Then there was the transfer to Chapter 2 in the not-too-distant past, where everything got updated including weapons, vehicles, and even the island setting itself. You don't want to get left behind in Fortnite when things move on, so having a decent grounding in the game will help you stay on top, and our Fortnite tips here are intended to do just that.
As it's now been up and running for several years, it may feel daunting for newer players to jump in at this stage and take on the veterans, but that doesn't have to be the case. With the right approach, the game is still completely accessible to fresher players, so if you learn how to play Fortnite and take on board our Fortnite tips then you're giving yourself the best opportunity to survive and, most importantly, have fun.

1. Know how to start
At the start of each round, pick out a promising spot from the bus - structures mean loot is more likely, but isolation will give you a bit of breathing space while you get set up on the ground. Prepare to be adaptable during this landing phase by keeping an eye on where the other players are skydiving to, and if you find them heading to your chosen location on the way down then break away, as it's going to get really messy upon landing otherwise. Your order of priorities upon landing should be as follows: Get out of the open. Get a weapon. Get collecting building resources.
One of the Fortnite tips I live by is waiting until the bus reaches the very edge of the map before jumping out. You're less likely to bump into other people and will have more of a chance of getting some undisturbed loot to set you up for the later stages of a match. Yes, you may have to travel a little further to get inside the circle as the storm closes in, but it's worth it for that great opening setup.

2. Nail the landing
When falling out of the bus, you may have seen some other opponents pulling out their gliders later than you. How? It all comes down to how high you are above land - for example, your glider will open a lot higher over a mountain than it would over a lake. So, choosing your path and approximating where your glider is going to open will get you ahead of your opponents to claim that well deserved chest. Falling over a low area will allow you to open your 'chute as late as possible, then if you swerve across to a hill or building you can land quicker and sprint off on foot. Check out our detailed Fortnite landing guide for more help in this discipline. 

3. Know your gear
Listen out for chests, as they make a magical tingling noise when you’re close by and should contain at least one useful, high level item and some of the best Fortnite new and unvaulted weapons. They’re usually hiding in attics or basements, so be prepared to smash stuff to get to them. Gear comes in six colours - learn them and you won’t waste your time trying to get to a blue item when you already have a set of purples:

Common - Grey
Uncommon - Green
Rare - Blue
Epic - Purple
Legendary - Orange
Mythic - Gold [only the Infinity Gauntlet and Infinity Blade so far]

4. Stay in cover as much as possible
Avoid running across open areas as the longer you stay in the open, the more likely you are to be spotted by another player. Stick to cover as much as possible, and if you do have to make a cross-country run, make sure you hug cover or use your jumps to make you a trickier target. You have unlimited sprint at your disposal so there's no excuse to dawdle! Also remember that bushes are your friends, so use them and use them often - although the actual bush disguise item might give you away in this situation by making you a larger target. Often you can camp in a pre-planted bush with your head popping out and enemies will just walk on by. Admittedly it’s not the most respectable way to get a kill, but when you’re low on health and have very little ammo then it’s a great way to get in the first shot or use it strategically to hide - it’s your call.

5. Constantly assess your surroundings
If you run into a new area and see construction has taken place, be very wary indeed as someone has set up a base here, and they might well still be around to defend it. Likewise, damaged buildings also reveal that you're not the first person to pass through here, so be on the lookout for an enemy presence. Listen for audio-cues constantly, as your ears are the best defence you have and will often give away an opponent's position before you've had chance to see them.

6. Use your consumables
After opening chests or raiding the supplies dropped by fallen foes, you should come across consumables to recover your health and shield. These are handy to have in reserve, but there's no point carrying them around for later if you can get the benefit from them immediately. You don't want to get taken out because you've got no shield and had a potion in your pocket the whole time. That's just embarrassing. 

One good Fortnite tip is that the better the item is, the longer it will take to consume, so make sure you're in cover and out of sight before using it. These are the benefits you can get from various consumables:

Shield Potion: +50 Shield, to a maximum of 100. Use time is 5 seconds. Maximum carry stack is 3
Small Shield Potion: +25 Shield, to a maximum of 50. Use time is 2 seconds. Maximum carry stack is 6
Medkit: Restores full Health. Use time is 10 seconds. Maximum carry stack is 3
Bandages: +15 Health, to a maximum of 75. Use time is 3 seconds. Maximum carry stack is 15
7. Remember to cover your tracks
Advertisement

For the love of goodness, close the door. Did you grow up in a barn? This is without doubt one of the most useful things you can do while exploring, as shutting doors allows you to hear intruders entering the building you're in. Think of it as an early warning system, giving you a few seconds to prepare and get the jump on your unsuspecting enemy while they move from room to room. Also, open doors tell others that someone has been through the building already, so don't give away your position unnecessarily by leaving them ajar.

It's also worth thinking about how you cut down trees on your quest for resources too. Try not to completely cut down trees and leave big trees with at least one hit left so you don't give yourself away when the tree disappears, or from the trail of stumps you've left in your wake. 

8. Use height to your advantage
Being higher than other players will 9 times out of 10 give you an advantage, so camp at the top of buildings, head up hills and build vertically - just make sure you have some cover up there. Be aware of your silhouette, as elevation is great for intermittent scans of the immediate area - and you should do this to keep apprised of all potential threats - but being framed against the skyline also makes you a lot more visible as you'll be susceptible to long range attacks. For more building tips and tricks be sure to check out our Fortnite building guide.

9. Build for defence
Learn the ‘instant combat tower’ structure, and map it to your muscle memory as quickly as possible. Four walls, a staircase in the middle, move up and repeat, to build a simple, effective, elevated fort that you can duck in and out of at will. Learn to treat building as part of your weapon and gear set - you can build on the fly and climb stairs the instant their construction outline appears, so make them a dynamic part of your game. Getting sniped while out in the open? Throw walls down between you and your attacker, to buy yourself time as you run for cover.

10. Use the Storm (yes, really)
Don’t fear the Storm shrinking for the initial few rounds as it gives you plenty of time to get inside the Eye at first, and even if you do find yourself in the danger zone your health drains pretty slowly. Once you’re into round 4 or 5 though it’s deadly, so watch your health and try not to get trapped too far into it. As you’re pushed closer to the middle of the map and the player count decreases, take more care to linger on the outskirts of the circle. There are fewer angles you can be attacked from there, and you can lie low while your opponents wear each other down. With your back to the edge of the Storm you're unlikely to get attacked from behind, so build this positioning into your tactics where possible.

Remember to watch where the Eye of the Storm is and how long is left on the clock until the next Storm Eye Shrinking phase, then plan your route ahead and make sure you leave yourself enough time to reach safety - there's nothing worse than being well equipped and on a good run, then dying in the Storm because you couldn't outrun its advances.

11. Prepare for the endgame
If you make it to the endgame, and are up against just a handful of players in a small area, blow up as much of the potential cover as you can. These final stand-offs tend to be a waiting game, where hidden players bait each other into exposing themselves, so don’t give them the option. If you’re lucky enough to obtain a Slurp Juice, save it as this item is most effective when you are about to engage in battle. If you see an enemy before they see you be sure to knock back your Slurp Juice, as this will regenerate both your armor and health for a short period of time, giving you a strong advantage over your opponent.
Destroying opponents' bases and stairways is a great way to force them out into the open or cause them to run in fear. When destroying a base, start from the ground up - if you destroy the base of a building the rest will collapse, causing an avalanche of materials and hopefully some broken legs. Your prey will then be out in the open, ready for you to pick off.

12. Vehicles are really, really, REALLY loud
The vehicles available were significantly overhauled for Chapter 2, and initially you could only ride on Fortnite motorboats. These can seat up to four players and fire missiles, while their boost can be used to traverse land as well as water - though this does cause a slow damage deterioration to the boat. They’re a much faster way to get from A to B, but they will alert anyone in the nearby vicinity to your location. Fortnite helicopters were added later, to provide an option for aerial travel, but they don't have any weapons built-in.

Seriously, vehicles are really loud. Only use them as a last resort because they don’t offer much protection either, which means anyone who sees you can get a few free shots in before you can dismount and build some cover. They’re a lot of fun, but it’s a high risk/low reward situation unless you’re certainly going to die without one.

13. Crouch as much as possible
If you're in a named location, there's a high possibility that you're not alone, so do your best to stay stealthy. Crouching down keeps you slow and quiet, meaning you can move around without drawing attention to yourself. You'll quickly hear anyone else that's around who's running or using their pickaxe to harvest resources, and in Fortnite having any kind of upper hand is always a good idea.

14. You can hear more if you play with headphones
Fortnite's excellent use of spatial audio is only enhanced when you're donning headphones, such as our recommended Fortnite headsets. Footsteps, gunshots and other in-game sounds will feel much more immersive and it'll be easier to identify which direction they're coming from if you're wearing headphones. Hearing another player before you see them is often half the Fortnite strategy, so give yourself an audio advantage.

15. Dead player drops can be a trap
Sometimes you'll spot a load of loot just lying on the ground, rather than in a chest or tucked away inside a building. It's a sign that some poor soul has been slain there, and although it might look like an inviting way to nab yourself some bandages and weaponry, be aware that the person that killed them may be lurking nearby ready to take you out too. 
Instead of just diving headfirst into that loot, hang back and survey the scene and once you're 100% sure the coast is clear, you can go grab some of that shiny stuff. 

16. Snack on foraged items
You'll find Fortnite foraged items in various areas around the map and you can nom them on the go, though unfortunately you can't shove a load in your pockets for later. An apple will give you five health, a mushroom will add five shield, a coconut awards five health or shield, and the newer Slurpshrooms add ten health or shield, which is great for when you can't find anything more than a small shield potion or are in desperate need of a health boost after a fight.

17. Turn on the Builder Pro controls in the settings
Hidden away in the settings menu are a load of controller settings (and PC ones too, of course), which will allow you to tweak various elements of your control layout. Switch to Builder Pro for speedier building, turn on Sprint Cancels Reloading, Tap to Search / Interact, Controller Auto-Run, Turbo Building and Auto Material Change. It's worth taking a look in here in between matches to make sure everything is as you'd want it to be to enhance the way you play

18. Building is the best weapon you have
We’ve already mentioned how important it is to harvest resources, but it cannot be understated how much it helps in a fight. If you try to peek and get a few shots on someone without building, they can easily get a few shots back. Instead, treat building like your ultimate weapon. You can always get into a better position than your adversary when building. Even if you think you can get a lot of shots off without building, don’t risk it. Build some decent cover and peek from behind that, take a few shots then build again. A fight taking 10 seconds longer but retaining full health is better than it being over quicker, but you’ve lost 90 health.

19. Reordering your stuff will make sure you're extra prepared for the fight
When you've got a little arsenal of weapons and other supplies, it pays to take a second to have a rummage in your backpack in Fortnite. Rearranging your wares so that all the weapons are together is a neat trick that'll make sure you can swap between a shotgun and an assault rifle, for example, with a quick click rather that having to scroll through bandages and shields to make the same move. To rearrange, open your backpack and select the first item, then move over to the item you want to swap it with and select that. Simple, but effective.

20. Only use ziplines if you're sure there's no enemies around
There are Fortnite ziplines criss-crossing the map on almost every mountain, and they're useful for getting to the top (or bottom) without risking damage or using any materials. There's only one problem...

You have no protection whatsoever, and you don't move very fast. Sure, you can shoot back while the high-powered human magnet carries you along but when you're a sitting duck, that isn't much help. Before using one to get anywhere, you're going to want to clear out the surrounding area as best as you can. There's nothing worse than hopping on a zipline then being shot in the back two seconds later.

