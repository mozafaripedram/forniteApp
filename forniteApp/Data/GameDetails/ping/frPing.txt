﻿Il y a un système de ping Fortnite maintenant, tout comme Apex Legends et l'homme qui était rapide
Un nouveau système de ping Fortnite vous permet d'appeler des armes, des munitions et plus encore à votre équipe
Epic a introduit un système de ping Fortnite dans tous les nouveaux trucs de Fortnite Season 8 qui vous permettent d'appeler des objets et des armes à d'autres personnes de votre équipe. Il s'agit d'une refonte du système World Marker que le jeu avait déjà en place et qui n'a pas de doublage, mais fonctionnellement, il est maintenant plus ou moins identique au système de ping Apex Legends tant apprécié.
Il n'est pas difficile de comprendre pourquoi: le système de ping utilisé dans Apex Legends était l'une des idées qui ont vraiment attiré l'attention des gens lorsque Respawn et la réponse d'EA au genre Battle Royale sont arrivés. Il n'est donc pas surprenant de voir Fortnite le copier de près pour rester à jour avec les nouveaux mécanismes que ses fans vont probablement commencer à poser. La seule surprise est la rapidité avec laquelle Fortnite a amélioré son système World Marker pour imiter Apex Legends.

La question est de savoir comment utiliser le système de ping Fortnite?

Que fait le système de ping Fortnite?
Le système de ping Fortnite appelle tout ce qui se trouve dans le monde avec un ping audible (attendez-le), ainsi qu'une icône d'identification et une distance visibles par les autres personnes de votre équipe. Il n'y a pas de doublage, mais cela vous permet toujours de mettre en évidence des choses comme les coffres, les munitions, les armes, etc., ainsi que les menaces, d'une manière que les membres de l'équipe peuvent facilement identifier.

Dans un monde idéal, cela devrait aider les escouades de randoms à mieux coordonner les choses sans avoir à utiliser des casques et à se parler. Que cela se produise ou non, c'est autre chose.

Comment utilisez-vous le système de ping Fortnite?
Le système de ping Fortnite vit sur le bouton gauche du D-pad. Lorsque vous appuyez dessus, il envoie un ping à tout ce que vous regardez avec un marqueur et une distance. Si vous ne regardez pas une arme ou tout autre objet, il marquera l'emplacement sur la carte sous votre réticule. Un double tap en revanche indiquera un danger - un moyen utile de signaler les escouades rivales.
