﻿Es gibt jetzt ein Fortnite-Ping-System, genau wie Apex Legends und ein Mann, der schnell war
Mit einem neuen Fortnite-Ping-System können Sie Ihrem Team Waffen, Munition und mehr rufen
Epic hat ein Fortnite-Ping-System in all das neue Fortnite Season 8-Material integriert, mit dem Sie anderen Personen in Ihrem Trupp Gegenstände und Waffen mitteilen können. Es ist eine Überarbeitung des World Marker-Systems, das das Spiel bereits eingerichtet hat, und es fehlt jede Sprachausgabe, aber funktionell ist es jetzt mehr oder weniger identisch mit dem vielgelobten Apex Legends-Ping-System.
Es ist nicht schwer zu verstehen, warum: Das in Apex Legends verwendete Ping-System war eine der Ideen, die die Aufmerksamkeit der Leute auf sich zogen, als Respawn und EAs Antwort auf das Battle Royale-Genre eintrafen. Es ist also nicht verwunderlich, dass Fortnite es genau kopiert, um über neue Mechaniken auf dem Laufenden zu bleiben, nach denen seine Fans wahrscheinlich fragen werden. Die einzige Überraschung ist, wie schnell Fortnite sein World Marker-System verbessert hat, um Apex Legends zu affen.

Die Frage ist, wie Sie das Fortnite-Ping-System verwenden.

Was macht das Fortnite-Ping-System?
Das Fortnite-Ping-System ruft alles auf der Welt mit einem hörbaren (darauf warten) Ping sowie einem Erkennungssymbol und einer Entfernung auf, die für andere Personen in Ihrem Team sichtbar sind. Es ist keine Sprachausgabe erforderlich, aber Sie können dennoch Dinge wie Truhen, Munition, Waffen usw. sowie Bedrohungen auf eine Weise hervorheben, die die Mitglieder des Trupps leicht identifizieren können.

In einer idealen Welt sollte es Random-Trupps helfen, die Dinge besser zu koordinieren, ohne Headsets verwenden und miteinander sprechen zu müssen. Ob das tatsächlich passiert oder nicht, ist eine andere Sache.

Wie benutzt man das Fortnite-Ping-System?
Das Fortnite-Ping-System befindet sich auf der linken D-Pad-Taste. Wenn Sie darauf drücken, pingt es mit einem Marker und einer Entfernung, was auch immer Sie sehen. Wenn Sie keine Waffe oder einen Gegenstand betrachten, wird der Ort auf der Karte unter Ihrem Fadenkreuz markiert. Ein doppeltes Tippen hingegen weist auf eine Gefahr hin - ein nützlicher Weg, um auf rivalisierende Trupps hinzuweisen.
