﻿Tous les détails sur les nouveaux emplacements de voitures Fortnite et où trouver les stations-service Fortnite

Les voitures Fortnite sont l'une des plus grandes mises à jour du jeu ces derniers temps, et après de nombreuses spéculations (plus un court retard), elles sont finalement apparues le mercredi 5 août. De toute évidence, maintenant elles sont réellement disponibles, la première question que les gens vont poser est "où sont les voitures Fortnite", et les joueurs voudront également savoir comment les conduire. Un nouveau mécanisme a été introduit, ce qui signifie que ces véhicules dépendent du carburant pour continuer à fonctionner, donc trouver et utiliser des stations-service Fortnite est désormais une partie importante de votre stratégie. Nous sommes ici pour fournir toutes les informations Fortnite sur la façon de trouver et de conduire des voitures, y compris où trouver les emplacements des stations-service Fortnite pour garder votre moteur en marche, dans notre guide complet des voitures Fortnite

Maintenant qu'ils sont enfin dans le jeu, vous pouvez trouver des voitures Fortnite dans de nombreux endroits de l'île. Nous avons marqué un nombre décent pour vous aider à démarrer en un rien de temps, mais la carte ci-dessus ne se veut pas exhaustive - honnêtement, vous n'aurez pas à chercher loin lorsque vous courrez pour en trouver un. Ne vous laissez pas berner par les véhicules équipés d'un collier de roue, car ce sont des parties fixes du paysage et ne peuvent pas être conduites.

-Comment conduire des voitures Fortnite
Les voitures Fortnite fonctionnent de la même manière que les autres véhicules de Fortnite, alors approchez-vous-en et interagissez pour monter à bord, puis vous pourrez changer de siège à votre guise et transporter vos coéquipiers dans les deux sens. Les commandes peuvent prendre un peu de temps pour s'y habituer, car le fait de pousser de haut en bas sur le joystick gauche affecte l'accélération et le freinage ainsi que les déclencheurs des pads. La direction est contrôlée en poussant à gauche et à droite sur le joystick gauche, mais gardez à l'esprit que votre véhicule essaiera également de conduire dans la direction à laquelle la caméra fait face afin que vous ne puissiez pas regarder autour de vous sans changer votre trajectoire de déplacement.

-Quelles voitures Fortnite sont disponibles?
Au lancement, quatre types différents de voitures Fortnite sont présents dans le jeu. Selon le texte de présentation, voici ce que vous devez savoir à leur sujet:

Islander Prevalent - L'esprit de responsabilité
Victory Motors Whiplash - Ce n'est pas seulement un nom. C'est un avertissement
OG Bear - Ne piquez pas l'ours
Garde-boue Titano - Prenez la route
Ces voitures Fortnite ont également des stations de radio intégrées pour écouter une variété de musique, y compris des pistes sous licence, bien qu'au moment de la rédaction de cet article, cela ait été temporairement désactivé pendant qu'un problème est en cours d'enquête.

-Emplacements des stations-service Fortnite
Les voitures Fortnite ont besoin d'essence pour continuer à conduire, ce qui signifie que les stations-service Fortnite joueront un rôle plus important que jamais dans le jeu. Nous avons marqué tous les emplacements des stations-service Fortnite sur la carte ci-dessus, et voici une liste des endroits où vous les trouverez:


A5: Houx Hedges
B3: Sables en sueur
J4: Sources salées
J2: Parc agréable
E2: à l'est de Pleasant Park
G2: à l'ouest de Steamy Stacks
G4: à l'est de Frenzy Farm
F5: au sud de Frenzy Farm
F6: Lac paresseux
G7: Catty Corner
J7: à l'ouest de Misty Meadows
C6: Au nord du marais Slurpy
B7: La Fortilla
Pour faire le plein d'une des voitures Fortnite, garez-vous simplement près d'une pompe à essence, puis sortez et interagissez avec elle - cela vous permet de gicler du carburant directement sur le véhicule pour le remplir. Si vous transportez un bidon d'essence, vous l'utilisez de la même manière, et si vous vous approchez d'une pompe à essence équipée, l'invite vous permettra de remplir le bidon.