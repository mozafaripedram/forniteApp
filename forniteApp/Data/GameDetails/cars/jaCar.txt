﻿新しいフォートナイトの車の場所とフォートナイトのガソリンスタンドの場所に関するすべての詳細

Fortnite車は、最近のゲームで発生する最大の更新の1つであり、多くの憶測（および少しの遅延）の後、8月5日水曜日にようやく登場しました。明らかに、人々が最初に尋ねる質問が実際に利用可能になりました。は「フォートナイトの車はどこにあるのか」であり、プレイヤーはそれらの運転方法も知りたいと思うでしょう。新しいメカニズムが導入されました。つまり、これらの車両は燃料に依存して走行を続けるため、Fortniteガソリンスタンドを見つけて使用することが戦略の重要な部分になりました。完全なフォートナイトカーガイドで、モーターを動かし続けるためのフォートナイトガソリンスタンドの場所を見つける場所など、車を見つけて運転する方法に関するすべてのフォートナイト情報を提供するためにここにいます

ついにゲームに登場したので、島のあちこちでFortniteの車を見つけることができます。私たちはあなたがピンチで始めるためにまともな数をマークしましたが、上の地図は網羅的であることを意図していません-あなたがそれを見つけるために走り回っているときあなたは正直に遠くを見る必要はありません。ただし、ホイールクランプが取り付けられている車両は、風景の固定部分であり、運転できないため、だまされないでください。

-Fortnite車の運転方法
Fortniteの車は、Fortniteの他の車と同じように機能するので、それに近づき、相互作用して乗り込みます。そうすれば、自由に座席を切り替えて、チームメートを前後にフェリーで運ぶことができます。左スティックを上下に押すと、パッドトリガーとともに加速とブレーキがかかるため、コントロールには少し慣れが必要です。ステアリングは左スティックを左右に押すことで制御されますが、車両はカメラが向いている方向にも運転しようとするため、移動軌道を変更せずに周囲を見渡すことはできません。

-どのFortnite車が利用できますか？
発売時には、ゲームには4種類のFortniteカーがあります。宣伝文によると、これはあなたがそれらについて知る必要があることです：

アイランダーが蔓延している-責任の精神
ビクトリーモーターズむち打ち症-それは単なる名前ではありません。警告です
OGクマ-クマを突くな
TitanoMudflap-道路を所有する
これらのFortnite車には、ライセンストラックを含むさまざまな音楽を聴くための無線局も組み込まれていますが、執筆時点では、問題の調査中に一時的に無効になっています。

-Fortniteガソリンスタンドの場所
Fortniteの車は運転を続けるためにガスを必要とします。つまり、Fortniteのガソリンスタンドはゲームでこれまで以上に重要な役割を果たします。上の地図ですべてのFortniteガソリンスタンドの場所にマークを付けました。これらの場所のリストは次のとおりです。


A5：ホリーヘッジ
B3：汗まみれの砂
D4：ソルティスプリングス
D2：プレザントパーク
E2：プレザントパークの東
G2：高温多湿のスタックの西
G4：フレンジーファームの東
F5：フレンジーファームの南
F6：レイジーレイク
G7：キャティコーナー
D7：ミスティメドウズの西
C6：SlurpySwampの北
B7：フォルティラ
Fortnite車の1台に燃料を補給するには、ガソリンポンプの近くに駐車してから出て、それと対話します。これにより、車両に直接燃料を噴射して燃料を補給できます。ガス缶を持っている場合は、同じように使用できます。ガスポンプを装備した状態で近づくと、プロンプトで缶を補充できます。