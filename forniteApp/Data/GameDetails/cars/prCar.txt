﻿Todos os detalhes sobre as novas localizações de carros Fortnite e onde encontrar postos de gasolina Fortnite

Os carros Fortnite são uma das maiores atualizações que aconteceram no jogo nos últimos tempos, e depois de muita especulação (mais um pequeno atraso) eles finalmente apareceram na quarta-feira, 5 de agosto. Obviamente, agora que eles estão realmente disponíveis, a primeira pergunta que as pessoas farão é "onde estão os carros Fortnite", e os jogadores também vão querer saber como dirigi-los. Um novo mecânico foi introduzido, o que significa que esses veículos dependem de combustível para continuar funcionando, então encontrar e usar os postos de gasolina Fortnite agora é uma parte importante de sua estratégia. Estamos aqui para fornecer todas as informações Fortnite sobre como encontrar e dirigir carros, incluindo onde encontrar postos de gasolina Fortnite para manter seu motor funcionando, em nosso guia completo de carros Fortnite

Agora que eles finalmente estão no jogo, você pode encontrar carros Fortnite em muitos locais ao redor da ilha. Marcamos um número decente para ajudá-lo a começar, mas o mapa acima não pretende ser exaustivo - você honestamente não terá que procurar muito quando estiver correndo para encontrar um. Não se deixe enganar por veículos que têm uma braçadeira de roda acoplada, pois são partes fixas do cenário e não podem ser dirigidos.

-Como dirigir carros Fortnite
Os carros Fortnite funcionam da mesma maneira que os outros veículos em Fortnite, então aproxime-se e interaja para entrar, então você poderá trocar de assento quando quiser e transportar seus companheiros de equipe para frente e para trás. Pode demorar um pouco para se acostumar com os controles, pois empurrar para cima e para baixo no manche esquerdo afeta a aceleração e a frenagem junto com os gatilhos do pad. A direção é controlada empurrando para a esquerda e para a direita no manípulo esquerdo, mas tenha em mente que seu veículo também tentará dirigir na direção para a qual a câmera está voltada para que você não possa olhar ao seu redor sem alterar sua trajetória de viagem.

-Que carros Fortnite estão disponíveis?
No lançamento, há quatro tipos diferentes de carros Fortnite presentes no jogo. De acordo com a sinopse, isso é o que você precisa saber sobre eles:

Islander Prevalent - O espírito de responsabilidade
Victory Motors Whiplash - Não é apenas um nome. É um aviso
Urso OG - Não cutuque o urso
Titano Mudflap - Domine a estrada
Esses carros Fortnite também têm estações de rádio integradas para ouvir uma variedade de músicas, incluindo faixas licenciadas, embora no momento da escrita isso tenha sido temporariamente desativado enquanto um problema estava sendo investigado.

- Localizações de postos de gasolina Fortnite
Os carros Fortnite precisam de gasolina para continuar dirigindo, o que significa que os postos de gasolina Fortnite terão um papel mais importante do que nunca no jogo. Marcamos todos os locais de postos de gasolina Fortnite no mapa acima e aqui está uma lista de onde você os encontrará:


A5: Holly Hedges
B3: Sweaty Sands
D4: Salty Springs
D2: Pleasant Park
E2: Leste de Pleasant Park
G2: West of Steamy Stacks
G4: Leste de Frenzy Farm
F5: Sul de Frenzy Farm
F6: Lago lento
G7: Catty Corner
D7: Oeste de Misty Meadows
C6: Norte do Pântano Slurpy
B7: The Fortilla
Para reabastecer um dos carros Fortnite, simplesmente estacione perto de uma bomba de gasolina, saia e interaja com ela - isso permite que você esguiche o combustível diretamente no veículo para abastecê-lo. Se você estiver carregando uma lata de gás, use-a da mesma maneira, e se você se aproximar de uma bomba de gás com ela equipada, o prompt permitirá que você recarregue a lata.