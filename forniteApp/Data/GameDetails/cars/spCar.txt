﻿Todos los detalles sobre las nuevas ubicaciones de automóviles de Fortnite y dónde encontrar las estaciones de servicio de Fortnite

Los autos de Fortnite son una de las actualizaciones más grandes que le han ocurrido al juego en los últimos tiempos, y después de mucha especulación (más un breve retraso) finalmente aparecieron el miércoles 5 de agosto. Obviamente, ahora están disponibles, la primera pregunta que la gente hará. es "dónde están los coches de Fortnite", y los jugadores también querrán saber cómo conducirlos. Se ha introducido una nueva mecánica, lo que significa que estos vehículos dependen del combustible para seguir funcionando, por lo que encontrar y usar las gasolineras Fortnite es ahora una parte importante de su estrategia. Estamos aquí para proporcionar toda la información de Fortnite sobre cómo encontrar y conducir automóviles, incluido dónde encontrar ubicaciones de estaciones de servicio de Fortnite para mantener el motor en funcionamiento, en nuestra guía completa de automóviles de Fortnite.

Ahora que finalmente están en el juego, puedes encontrar autos Fortnite en muchos lugares de la isla. Hemos marcado un número decente para que pueda comenzar en un apuro, pero el mapa de arriba no pretende ser exhaustivo; honestamente, no tendrá que mirar muy lejos cuando esté corriendo para encontrar uno. Sin embargo, no se deje engañar por los vehículos que tienen fijada una abrazadera de rueda, ya que son partes fijas del escenario y no se pueden conducir.

-Cómo conducir autos Fortnite
Los autos de Fortnite funcionan de la misma manera que los otros vehículos en Fortnite, así que acérquese e interactúe para subir, luego podrá cambiar de asiento a su gusto y transportar a sus compañeros de equipo de un lado a otro. Los controles pueden tomar un poco de tiempo para acostumbrarse, ya que presionar hacia arriba y hacia abajo en la palanca izquierda afecta la aceleración y el frenado junto con los gatillos de la almohadilla. La dirección se controla presionando hacia la izquierda y hacia la derecha en el joystick izquierdo, pero tenga en cuenta que su vehículo también intentará conducir en la dirección en la que mira la cámara para que no pueda mirar a su alrededor sin cambiar su trayectoria de viaje.

-¿Qué coches de Fortnite están disponibles?
En el lanzamiento, hay cuatro tipos diferentes de autos Fortnite presentes en el juego. Según la propaganda, esto es lo que necesita saber sobre ellos:

Islander Prevalent - El espíritu de responsabilidad
Victory Motors Whiplash: no es solo un nombre. Es una advertencia
OG Bear - No pinches al oso
Guardabarros Titano - Posee la carretera
Estos autos de Fortnite también tienen estaciones de radio integradas para escuchar una variedad de música, incluidas pistas con licencia, aunque en el momento de escribir este artículo se ha desactivado temporalmente mientras se investiga un problema.

-Ubicaciones de las gasolineras Fortnite
Los autos de Fortnite requieren gasolina para seguir conduciendo, lo que significa que las estaciones de servicio de Fortnite jugarán un papel más importante que nunca en el juego. Hemos marcado todas las ubicaciones de las estaciones de servicio de Fortnite en el mapa de arriba, y aquí hay una lista de dónde las encontrará:


A5: Setos de acebo
B3: Arenas sudorosas
D4: manantiales salados
D2: Parque agradable
E2: Este de Pleasant Park
G2: al oeste de Steamy Stacks
G4: Al este de Frenzy Farm
F5: Al sur de Frenzy Farm
F6: lago perezoso
G7: rincón malicioso
D7: Oeste de Misty Meadows
C6: Al norte de Slurpy Swamp
B7: La Fortilla
Para repostar uno de los autos de Fortnite, simplemente estacione cerca de una bomba de gasolina y luego salga e interactúe con ella; esto le permite rociar combustible directamente en el vehículo para llenarlo. Si lleva una lata de gasolina, úsela de la misma manera, y si se acerca a una bomba de gasolina equipada, el mensaje le permitirá volver a llenar la lata.