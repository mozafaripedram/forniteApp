//
//  FileReader.swift
//  Maradona
//
//  Created by pedram on 11/26/20.
//

import Foundation


enum lang : String{
    case fr = "fr"
    case en = "en"
    case gr = "gr"
//    case it = "it"
    case ru = "ru"
    case ar = "ar"
    case pr = "pr"
    case ch = "ch"
    case sp = "sp"
    case ja = "ja"
}



enum ext : String{
    case tip = "20"
    case car = "Car"
    case ping = "Ping"
    case level = "Level"
    case play = "Play"
}



class FileReader{
    static let shared = FileReader()
    
    let fileName = ["french.txt","english.txt","german.txt","italian.txt","rusi.txt","arabic.txt","torki.txt","spanish.txt","porteghali.txt"]
    
    func getText(lang : lang, ext : ext)-> String{
        let file = lang.rawValue + ext.rawValue
        var text = "some text"
        print(file)
        let path = Bundle.main.path(forResource: file , ofType: "txt")
        do {
            text = try String(contentsOfFile: path!)
            return text
        }catch{
            print(error.localizedDescription)
        }
        
        return text
    }
}
