﻿Here’s how to play Fortnite: the most basic tips for beginners starting off in the free battle royale extravaganza
Receiving some advice on how to play Fortnite before you rush headlong into the battle royale can help keep you alive for longer, especially if you're taking some of the extra spare time many of us have right now to make your first trip into the game. We know it can be intimidating to arrive as a fresh recruit, particularly when there's an existing player base of millions around the globe who've been battling for months or even years already, but it's never too later to try it out and see how you get on. We've put together this handy Fortnite beginner's guide to steer you in the right direction, so you won't be caught completely off-guard when you land on the island.
The game continues to evolve with Fortnite Chapter 2 Season 5, and although the island has changed significantly the core mechanics are generally still the same – so this Fortnite beginner's guide will help lead you in the right direction towards your first Victory Royale. Read on for plenty of Fortnite tips and tricks that will demonstrate how to play Fortnite to your strengths, so take your seat on the battle bus, thank the driver, and get ready to leap into battle..

1. You can’t take the things you pick up on Spawn Island with you
When you spawn into Spawn Island you’ll see guns, ammo, and building materials scattered around just waiting to be grabbed. And you can grab them - but they won’t come with you into the Fortnite map. Treat Spawn Island as a practise area for how to play Fortnite, although you’ll probably only be there for a minute at most while the game fills up with 100 players. You can shoot anyone in the vicinity but the guns won’t do any damage, or if you’re being a temporary pacifist, try your hand at building a quick sniping hut (more details on how to do that below in point 8). 

2. Wait as long as possible to drop from the Battle Bus
The Battle Bus’ horn will honk as soon as you’re able to drop from it onto the map below, but resist the urge to join the crowd of players who leap from it asap. Instead wait until there’s about 3 seconds to go before the Battle Bus reaches the end of its flight path, then leap out. You’ll have little to no players to compete with for landing spots, which means there’s a smaller chance of getting gunned in the face in your first two minutes of the match and less competition for loot. Aim towards a house or structure when you’re gliding as there’s a high chance of a chest being inside it, or at the very least some basic loot. Just hit the roof with your pickaxe to get in. Speaking of the glider...

3. Your glider deploys automatically
When you’re plummeting down from the Battle Bus, there is no way to make your glider deploy later as it automatically opens when you reach a certain height over the ground below. That’s all there is to it. It only folds up once you’ve landed, so just stay patient and make a beeline for the nearest structure.

4. Drink small shield potions before glugging a large one
Around the map you’ll find both small and large blue bottles, which give you 25 shield and 50 shield respectively if you use your right trigger to drink them (the same button you use for firing your gun). Once you have 50 or higher shield you can’t consume any more small bottles, so make sure to drink them first so you don’t have them burning up a slot in your inventory. You can chug large shield potions no matter the level your shield is at, so if you only have one of them go ahead: pop the lid, and gulp it down asap.

5. Assault rifles or SMGs are good beginner weapons
As a general rule, stick to assault rifles or SMGs when you’re first figuring out how to play Fortnite. Sniper rifles are useless under 75 metres, so although you’ll want to keep one handy do not use it in close quarters combat unless you absolutely have to. Another thing to bear in mind when you’re figuring out how to play Fortnite is if/when you’re shooting someone up close and personal, you’ll want to prioritise your shotgun. Shotguns - surprise surprise - do a ton of damage and are good for one-hit kills, so keep one in your hands when you’re exploring houses, basements, or any other small spaces.

6. Pay attention to the rarity scale
Grey guns are the most common, with green, blue, purple, and gold being the ascending order of rarity. Gold guns like the RPG and SCAR assault rifle are incredibly powerful, so don’t pass them up if you see them lying around. Hit Up on the D-pad to bring up your inventory, and if you hover over the guns you’ll be able to see how much damage they do if you’re having trouble deciding which to keep. 

7. Play with headphones on
Make sure you have a pair of decent headphones nestled on your ears, as hearing the sound of footsteps (or, more likely, gunshots) and being able to figure out what direction they’re coming from can be the difference between life and respawning in Fortnite. Knowing which direction players are coming from gives you valuable seconds to prepare, whether that’s switching to a shotgun or building a quick bit of cover.

8.  Build cover before you heal
While I’m on the subject of building, always, always, always build walls around you before you start to heal. Both healing and drinking shield potions take valuable seconds to consume (up to 10 seconds for a large healing kit), during which time you can’t move around, shoot, or do anything really except for twirl the camera. Which means you’re very vulnerable indeed, so those walls will stop any bullets connecting with your face. Walls can also clip into cliffs and hills, so there’s no need to find an empty bit of land.

9. There’s fall damage above 3 storeys
This isn’t Borderlands: falling from a substantial height (above three storeys, to be precise - that’s three of the standard walls one above the other) will take a chunk of your health, so either build ramps downwards or try to slide down hills or cliffs. 

10.  Always try to get the upper ground in a fight
When things start to get sticky and you find yourself in a gunfight with one of the 100 people running around the map, you want to be as high as possible. No, not in that way. Make sure you are above your opponent either by building ramps up into the sky, or by jumping repeatedly (which has the added bonus of making you harder to hit). A good strategy is to build four walls around yourself in a square, then build a ramp up to the wall facing your enemy. This’ll provide you with a good sniping platform that you can retreat down when you’re reloading. Just repeat the building formula upwards (jump to build below your feet) until you’re in a makeshift tower and have the upper hand – and gun, and grenade, and so forth...

11. Keep grenades, bandages, or shield potions in your inventory
It’s worth having at least one of the above items in your toolbar. Grenades are a great way to destroy some of your opponent’s cover, especially if you aim for the lowest part of their structure. Once the lowest level is destroyed, the rest of it will come tumbling down, leaving them out in the open and ready for your hail of bullets. Extra healing and shields will come in handy for when you’re backed into a corner but aren’t ready to give up, or you can share them with your squadmate if they’re in need of some first aid sharpish. 

12. Don’t destroy trees completely
Using your pickaxe to destroy trees is an essential way to get wood for building materials, but do not destroy trees completely. Instead stop when the tree has 50 health left on its hitbar, as leaving a trail of destroyed vegetation in your wake makes it incredibly easy for other players to follow your tracks and hunt you down. 

13. There are no longer different types of bombs
In the past we've had all sorts of different bombs to try out – smoke bombs, boogie bombs, stink bombs, shadow bombs, sticky bombs – which all became a bit tricky to keep track of. However, things have been massively simplified in Chapter 2 as there are now only standard grenades available, and we all know how they work. Time will tell if different varieties of bomb start to return to the game, but for now there's just good old fashioned grenades to throw.

14. Make sure you've selected the right mode from the lobby
When you boot up the game it's automatically going to try and drop you into a squad with all the slots filled by random players. You may well want to go solo or drop into a duo instead, so just toggle the game mode first. It'll say just above "Play!" whatever mode you're in. 

15. Crouching minimises the noise your feet make
Sneaky is as sneaky does, and being stealthy can be a great way to edge yourself closed to that coveted Battle Royale. Keeping yourself hidden is part of the trouble, but if you move while crouched you'll move much more quietly than if you run. It's a very useful trick if you can hear someone nearby, because you can manoeuvre yourself into a better hiding place before they come after you. 

16. You have to buy the Battle Pass to get the majority of the rewards
Although there are some perks available without spending a single dime, if you want to get the best skins and other items you'll need the Battle Pass. It costs 950 V-Bucks, meaning you'll need to buy the smallest V-Bucks bundle, at $9.99 / £7.99. After that it's just a case of playing and ticking off as many challenges as you can to start earning yourself some gear. 

17. You can break (almost) anything with your pickaxe
If you think you've got stuck in a building or built yourself inside a construction, don't fret. You can break (almost) anything with your pickaxe, so just start smashing your way out. But be careful, that little red circle icon that pops up means you're making noise and anyone nearby will hear it, so take your time escaping your current conundrum. 

18. You can change your skin, stickers, and emotes in the locker
All the skins, banner icons, gliders, harvesting tools, loading screen, contrails, emotes, dances, sprays and more all live in your Locker. You can switch them in and out from here before every match, but you won't be able to change any of it in match, so make sure you're happy with your look and your equipped items before you drop. 

19. Use Battle Lab mode to familiarise yourself with the map
One of the key aspects to improving at Fortnite is learning the map. Jumping into public games and fighting against 99 other players each time can get quite tiresome if you're not succeeding, so hop into Battle Lab mode then choose Create A Battle Lab to explore the map to your heart's content. You have free rein of the battle royale island, from Slurpy Swamp to Steamy Stacks and everywhere in between. All possible chests and vehicles are guaranteed to spawn in Battle Lab too, so you can learn where each chest is likely to be and familiarise yourself with your favourite locations.

20. Get into the habit of harvesting materials as you run
While the player with the best aim will usually come out on top in a fight, building is absolutely key to give yourself the biggest advantage possible. To build though, you need to have materials which can only be obtained through harvesting materials. Wood is the most common and you can destroy everything from trees and bushes to chairs and wardrobes. Test out which structures give you the most materials then harvest them as you run from one location to the next to give yourself maximum resources.

