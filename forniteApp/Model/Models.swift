//
//  Models.swift
//  Among Us
//
//  Created by pedram on 11/25/20.
//

import Foundation




class Video : Codable{
    var isnew : Bool = false
    var _id = ""
    var name = ""
    var link = ""
    var cat = ""
}

struct Wallpaper  : Codable{
    var isnew = true
    var _id = ""
    var name = ""
}

class Emote : Codable{
    var isnew = false
    var name = ""
    var _id = ""
    var link = ""
}

struct Series {
    var id = ""
}

struct Info : Codable{
    var score = 0
    var scorePerMin = 0.0
    var scorePerMatch = 0.0
    var wins = 0
    var top3 = 0
    var top5 = 0
    var top6 = 0
    var top10 = 0
    var top12 = 0
    var top25 = 0
    var kills = 0
    var killsPerMin = 0.0
    var killsPerMatch = 0.0
    var deaths = 0
    var kd = 0.0
    var matches = 0
    var winRate = 0.0
    var minutesPlayed = 0
    var playersOutlived = 0
    var lastModified = ""
}
