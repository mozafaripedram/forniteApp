//
//  Enums.swift
//  Among Us
//
//  Created by pedram on 11/25/20.
//

import Foundation


enum Category : String {
    case fun = "fun"
    case strategy = "strategy"
    case series = "series"
}
