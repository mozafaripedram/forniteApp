//
//  File.swift
//  Among Us
//
//  Created by pedram on 12/4/20.
//

import Foundation


class UserSettings: NSObject {
    
    static let defualt = UserDefaults.standard

    
    static func setLang(lang : lang){
        self.defualt.setValue(lang.rawValue , forKey : "language")
    }
    static func getLang()-> lang{
        let l = lang(rawValue: defualt.string(forKey: "language") ?? "en")
        return l!
    }
    
    
    
    static var numberOfEntrance : Int{
        set{
            defualt.setValue(newValue, forKey: "numberOfEntrance")
        }
        get{
            return defualt.integer(forKey: "numberOfEntrance")
        }
    }
    
    static var didRate : Bool{
        set{
            defualt.setValue(newValue, forKey: "didRate")
        }
        get{
            return defualt.bool(forKey: "didRate")
        }
    }
    
    static var favImages : [String]{
        set{
            defualt.setValue(newValue, forKey: "favImages")
            
        }
        get{
            return defualt.array(forKey: "favImages") as? [String] ?? [String]()
        }
    }
    
    static var favVid : [String]{
        set{
            defualt.setValue(newValue, forKey: "favVid")
        }
        get{
            return defualt.array(forKey: "favVid") as? [String] ?? [String]()
        }
    }
    
    
    static var userName : String{
        set{
            defualt.setValue(newValue, forKey: "userName")
        }
        get{
            return defualt.string(forKey: "userName") ?? ""
        }
    }
    
    static var userInfo : String{
        set{
            defualt.setValue(newValue, forKey: "userInfo")
        }
        get{
            return defualt.string(forKey: "userInfo") ?? ""
        }
    }
    
    static var isLogIn : Bool{
        set{
            defualt.setValue(newValue, forKey: "isLogIn")
        }
        get{
            return defualt.bool(forKey: "isLogIn")
        }
    }
    
    static var coins : Int {
        set{
            defualt.setValue(newValue, forKey: "coins")
        }
        get{
            return defualt.integer(forKey: "coins")
        }
    }
    
    
    static var friendsID : [String]{
        set{
            defualt.setValue(newValue, forKey: "friendsID")
        }
        get{
            return defualt.array(forKey: "friendsID") as? [String] ?? [String]()
        }
    }
    
    
    static var favFriendsID : [String]{
        set{
            defualt.setValue(newValue, forKey: "favFriendsID")
        }
        get{
            return defualt.array(forKey: "favFriendsID") as? [String] ?? [String]()
        }
    }
    
    static var userImg : String{
        set{
            defualt.setValue(newValue, forKey: "userImg")
        }
        get{
            return defualt.string(forKey: "userImg") ?? ""
        }
    }
    
    
    
    
    
    
    
    
}
