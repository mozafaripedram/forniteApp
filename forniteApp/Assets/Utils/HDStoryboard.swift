//
//  HDStoryboard.swift
//  Hyper
//
//  Created by @hd on 9/1/17.
//  Copyright © 2017 @hd. All rights reserved.
//

import UIKit

class HDStoryboard: NSObject {

    var storyboard: UIStoryboard?
    var storyboard2: UIStoryboard?
    var storyboard3: UIStoryboard?
    
    static let shared = HDStoryboard()
    
    
    override init() {
        super.init()
        storyboard = UIStoryboard(name: "Main", bundle: nil)
        storyboard2 = UIStoryboard(name: "Stats", bundle: nil)
        storyboard3 = UIStoryboard(name: "OnlyStat", bundle: nil)
        
    }
    
    func child(withIdentifier id: String) -> UIViewController? {
        return storyboard?.instantiateViewController(withIdentifier: id)
    }
    
    func child2(withIdentifier id: String) -> UIViewController? {
        return storyboard2?.instantiateViewController(withIdentifier: id)
    }
    
    func child3(withIdentifier id: String) -> UIViewController? {
        return storyboard3?.instantiateViewController(withIdentifier: id)
    }
    

    
    
}
