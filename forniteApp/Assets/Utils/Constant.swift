//
//  Constant.swift
//  Among Us
//
//  Created by pedram on 11/25/20.
//

import Foundation


//typealias
typealias completionBool = (_ success : Bool) -> Void
typealias completionViedos = (_ success : [Video]) -> Void
typealias completionWallpaper = (_ success : [Wallpaper]) -> Void
typealias completionSeries = (_ success : [Series]) -> Void
typealias completionStringArray = (_ success : [String]) -> Void
typealias completionEmote = (_ success : [Emote]) -> Void
typealias completionInfo = (_ status : Int , _ info : Info?) -> Void


//URLS
let MAINURL = "http://a2mp.site/fortnite/"

let VIDEO_URL = "\(MAINURL)videos/"
let STICEKR_URL = "\(MAINURL)sticker/"
let WALLPAPER_URL = "\(MAINURL)wallpaper/"
let GET_WALLPAPER_URL = "\(WALLPAPER_URL)download/"
let GET_STICKER_URL = "\(STICEKR_URL)download/"
let GET_COVER_VEIDO_URL = "\(VIDEO_URL)cover?id="
let GET_COVER_SERIES = "\(VIDEO_URL)cover?serieName="
let GET_VIDEO_CATEGORY = "\(VIDEO_URL)cat"
let GET_VIDEO_BY_CATEGORY = "\(VIDEO_URL)list?cat="
let GET_EMOTE = "\(MAINURL)emots/list"
let EMOTE_SEARCH = "\(GET_EMOTE)?search="



