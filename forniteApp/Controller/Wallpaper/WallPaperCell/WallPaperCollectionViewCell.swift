//
//  WallPaperCollectionViewCell.swift
//  Among Us
//
//  Created by pedram on 11/23/20.
//

import UIKit
import Kingfisher

class WallPaperCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var wallPaperImg: UIImageView!
    @IBOutlet weak var isNewImg: UIImageView!
    @IBOutlet weak var favBtn: UIButton!
    
    var selectedImg : UIImage = #imageLiteral(resourceName: "starS")
    var unselectedImg : UIImage = #imageLiteral(resourceName: "star")
    
    
    var wallpaper : Wallpaper!{
        didSet{
            do{
                
                let jsonEncoder = JSONEncoder()
                let jsonData = try jsonEncoder.encode(wallpaper)
                let json = String(data: jsonData, encoding: String.Encoding.utf8)
                jsonImg = String(json!.nsString)
                print(String(json!.nsString))
            }catch{
                print(error.localizedDescription)
            }
        }
    }
    var jsonImg : String!
    
    func configCell(wallpaper : Wallpaper){
        print(wallpaper.isnew)
        self.wallpaper = wallpaper
        self.wallPaperImg.setWallpaper(id: wallpaper._id)
        let img = isInFav() ? selectedImg : unselectedImg
        self.favBtn.setImage(img, for: .normal)
//        self.isNewImg.isHidden = !wallpaper.isnew
    }
    
    
    
    func isInFav() -> Bool{
        let fav = UserSettings.favImages
        for item in fav{
            if item == jsonImg{
                return true
            }
        }
        return false
    }
    
    @IBAction func favBtnPressed(_ sender: Any) {
        if isInFav(){
            var favImages = UserSettings.favImages
            favImages.removeAll(jsonImg)
            UserSettings.favImages = favImages
        }else{
            var favImages = UserSettings.favImages
            favImages.append(self.jsonImg)
            UserSettings.favImages = favImages
        }
        
        let img = isInFav() ? selectedImg : unselectedImg
        self.favBtn.setImage(img, for: .normal)
    }
    
    
    
}
