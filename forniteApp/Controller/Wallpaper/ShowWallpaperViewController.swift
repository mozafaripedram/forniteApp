//
//  ShowWallpaperViewController.swift
//  Maradona
//
//  Created by pedram on 11/26/20.
//

import UIKit
import StoreKit
import GoogleMobileAds
import Photos

class ShowWallpaperViewController: UIViewController , GADInterstitialDelegate {

    @IBOutlet weak var backImage: UIImageView!
    var interstitial: GADInterstitial!

    
    var imag : Wallpaper!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backImage.setWallpaper(id: imag._id)
        interstitial = createAndLoadInterstitial()
    }
    
    
    func configView(image : Wallpaper){
        self.imag = image
    }
    
    @IBAction func downloadBtn(_ sender: Any) {
        if backImage.image == nil {
            return
        }
        if UserSettings.coins < 1{
            //show user puchaseView
            let vc = HDStoryboard.shared.child2(withIdentifier: "coinVC")
            self.present(vc!, animated: true, completion: nil)
        }
        else{
            UserSettings.coins -= 1
            NotificationCenter.default.post(name: Notification.Name("coinChange"), object: nil)
        }
        UIImageWriteToSavedPhotosAlbum(backImage.image!, nil, nil, nil)
        let alert = UIAlertController(title: "Photo Saved", message: "photo saved to gallery", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            if !UserSettings.didRate{
                UserSettings.didRate = true
                if #available(iOS 10.3, *) {
                    SKStoreReviewController.requestReview()
                } else {
                    // Fallback on earlier versions
                }
            }else{
                if self.interstitial.isReady{
                    self.interstitial.present(fromRootViewController: self)
                }
            }
            
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
   

    func createAndLoadInterstitial() -> GADInterstitial {
      var interstitial = GADInterstitial(adUnitID: "ca-app-pub-6545436330357450/2129657519")
      interstitial.delegate = self
      interstitial.load(GADRequest())
      return interstitial
    }

    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
      interstitial = createAndLoadInterstitial()
    }

    @IBAction func closeBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
