//
//  WallPaperViewController.swift
//  Among Us
//
//  Created by pedram on 11/23/20.
//

import UIKit
import SwiftyJSON

class WallPaperViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource {
    
    

    @IBOutlet weak var botCons: NSLayoutConstraint!
    @IBOutlet var bottomCons: UIView!
    @IBOutlet weak var collection: UICollectionView!
    
    var images = [Wallpaper]()
    var isFav = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.changeBackGorundColor()

        collection.dataSource = self
        collection.delegate = self
        
        let layout = UICollectionViewFlowLayout()
        
        let w = HDUtils.width() / 2 - 16
        let h = HDUtils.height() / 3 - 16
//        self.navigationController?.hidesBarsOnTap = true
        
        layout.itemSize = CGSize(width: w, height: h)
        layout.sectionInset = UIEdgeInsets(top: 30, left: 8, bottom: 30, right: 8)
        layout.minimumLineSpacing = 16
        layout.minimumInteritemSpacing = 8
        collection.collectionViewLayout = layout
        collection.backgroundColor = .clear
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.post(name: Notification.Name("coinChange"), object: nil)

        if isFav{
//            self.botCons.constant = -40
            let fav = UserSettings.favImages
            images.removeAll()
            for item in fav{
                let temp = JSON(item)
                do {
                    self.images.append( try JSONDecoder().decode(Wallpaper.self, from: temp.description.data(using: .utf8)!))
                }catch{
                    print(error.localizedDescription)
                }
            }
        }else{
            APIService.getWallpapers(skip: nil) { (walls) in
                self.images = walls
                self.collection.reloadData()
            }
        }
    }
    
    
    func configView(){
        self.isFav = true
    }
    

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collection.dequeueReusableCell(withReuseIdentifier: "wallpaperCell", for: indexPath) as! WallPaperCollectionViewCell
        cell.configCell(wallpaper: self.images[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
        cell.contentView.backgroundColor = .clear
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = HDStoryboard.shared.child(withIdentifier: "showWallVC") as! ShowWallpaperViewController
        vc.configView(image: self.images[indexPath.row])
        self.present(vc, animated: true, completion: nil)
    }

}
