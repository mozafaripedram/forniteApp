//
//  CoinView.swift
//  forniteApp
//
//  Created by pedram on 1/15/21.
//

import UIKit

class CoinView: UIView {

    override func didMoveToWindow() {
        for item in self.subviews{
            if let label = item as? UILabel{
                label.font = UIFont(name: "Poppins-Bold", size: 14)
                label.text = "\(UserSettings.coins)"
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("coinChange"), object: nil)

    }


    @objc func methodOfReceivedNotification(notification: Notification) {
        for item in self.subviews{
            if let label = item as? UILabel{
                label.text = "\(UserSettings.coins)"
            }
        }
    }

}
