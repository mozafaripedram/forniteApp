//
//  VideoPlayerViewController.swift
//  forniteApp
//
//  Created by pedram on 12/31/20.
//

import UIKit
import youtube_ios_player_helper

class VideoPlayerViewController: UIViewController , YTPlayerViewDelegate{

    @IBOutlet weak var playerView: YTPlayerView!
    
    var video : Video!
    var emote : Emote!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var link = video == nil ? emote.link : video.link
        link = link.replacingOccurrences(of: "https://www.youtube.com/watch?v=", with: "")
        link = link[0..<11]
        playerView.delegate = self
        playerView.load(withVideoId: link)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
//        let link = video == nil ? emote.link : video.link
//        print(link)
//        playerView.delegate = self
//        playerView.loadVideo(byURL: link, startSeconds: 2)
    }
    func playerView(_ playerView: YTPlayerView, receivedError error: YTPlayerError) {
        print(error)
    }
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        playerView.playVideo()
    }
    func playerView(_ playerView: YTPlayerView, didPlayTime playTime: Float) {
        print("we are in")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
