//
//  SettingsViewController.swift
//  Among Us
//
//  Created by pedram on 11/24/20.
//

import UIKit
import SwifterSwift
import StoreKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var singBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        changeBackGorundColor()
        self.usernameLbl.text = UserSettings.userName
        self.profileImg.image = UIImage(named: UserSettings.userImg)
        if !UserSettings.isLogIn{
            self.singBtn.setImage(UIImage(), for: .normal)
            self.singBtn.setTitle("Sing In", for: .normal)
            self.usernameLbl.isHidden = true
            self.profileImg.isHidden = true
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.usernameLbl.text = UserSettings.userName
        self.profileImg.image = UIImage(named: UserSettings.userImg)
        if !UserSettings.isLogIn{
            self.singBtn.setImage(UIImage(), for: .normal)
            self.singBtn.setTitle("Sing In", for: .normal)
            self.usernameLbl.isHidden = true
            self.profileImg.isHidden = true
        }else{
            self.singBtn.setImage(UIImage(named:"Group 1789"), for: .normal)
//            self.singBtn.setTitle("Log out", for: .normal)
            self.usernameLbl.isHidden = false
            self.profileImg.isHidden = false
        }
    }
    
    
    
    
    @IBAction func rateUsBtnPressed(_ sender: Any) {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    @IBAction func favBtnPressed(_ sender: Any) {
        let vc = HDStoryboard.shared.child(withIdentifier: "favVC")
        self.present(vc!, animated: true, completion: nil)
    }
    
//    let availableLanguages = ["Arabic" , "English" , "French" , "German", "Italian" , "Portuguese", "Russian" , "Latin", "Persian" , "Chinese", "Korean"]
//    let ekhtesar: [lang] = [ .ar , .en ,.fr ,.gr ,.it ,.pr ,.ru ,.la , .fa , .ch  ]
//
    @IBAction func shareUsBtn(_ sender: Any) {
        
        if #available(iOS 13.0, *) {
            let firstActivityItem = "Check out this amazing Fortinte guide app!"

            // Setting url
            let secondActivityItem : NSURL = NSURL(string: "https://apps.apple.com/us/app/best-emotes-tips-for-fortnite/id1545919734")!
            
            let activityViewController : UIActivityViewController = UIActivityViewController(
                activityItems: [firstActivityItem, secondActivityItem], applicationActivities: nil)
            
            
            activityViewController.activityItemsConfiguration = [
                UIActivity.ActivityType.message
            ] as? UIActivityItemsConfigurationReading
            
            // Anything you want to exclude
            activityViewController.excludedActivityTypes = [
                UIActivity.ActivityType.postToWeibo,
                UIActivity.ActivityType.print,
                UIActivity.ActivityType.assignToContact,
                UIActivity.ActivityType.saveToCameraRoll,
                UIActivity.ActivityType.addToReadingList,
                UIActivity.ActivityType.postToFlickr,
                UIActivity.ActivityType.postToVimeo,
                UIActivity.ActivityType.postToTencentWeibo,
                UIActivity.ActivityType.postToFacebook,
                UIActivity.ActivityType.postToWhatsApp,
                UIActivity.ActivityType.postToTwitter
            ]
            
            activityViewController.isModalInPresentation = true
            
            self.present(activityViewController, animated: true, completion: nil)
            
            
        }else{
            let alert = UIAlertController(title: "This button not available.", message: "Stickers are avalibale in imessage", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in}))
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    
    var actionSheet: UIAlertController!
    
    @IBAction func languageBtnPressed(_ sender: Any) {
//        
//        actionSheet = UIAlertController(title: nil, message: "Switch Guide Language", preferredStyle: UIAlertController.Style.actionSheet)
//        for language in 0...availableLanguages.count - 1{
//            let displayName = availableLanguages[language]
//            let languageAction = UIAlertAction(title: displayName, style: .default, handler: {
//                (alert: UIAlertAction!) -> Void in
//                UserSettings.setLang(lang: self.ekhtesar[language])
////                self.text.text = FileReader.shared.getText(lang: self.ekhtesar[language])
//                
//            })
//            actionSheet.addAction(languageAction)
//        }
//        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {
//            (alert: UIAlertAction) -> Void in
//        })
//        actionSheet.addAction(cancelAction)
//        if let popoverController = actionSheet.popoverPresentationController {
//            popoverController.sourceView = self.view //to set the source of your alert
//            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
//            popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
//        }
//        self.present(actionSheet, animated: true, completion: nil)
        
        
    }
    
    @IBAction func purchaseBtnPressed(_ sender: Any) {
        let vc = HDStoryboard.shared.child2(withIdentifier: "coinVC")
        self.present(vc!, animated: true, completion: nil)
    }
    
    @IBAction func signBtnPressed(_ sender: Any) {
        UserSettings.isLogIn = false
        let vc = HDStoryboard.shared.child2(withIdentifier: "loginVC")
        vc?.modalPresentationStyle = .fullScreen
        self.present(vc!, animated: true, completion: nil)
        
    }
    
    @IBAction func profileImgBtnPressed(_ sender: Any) {
        
    }
    
    
    

}
