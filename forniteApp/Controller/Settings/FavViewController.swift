//
//  FavViewController.swift
//  forniteApp
//
//  Created by pedram on 12/23/20.
//

import UIKit
import Pageboy
import Tabman

class FavViewController: TabmanViewController, PageboyViewControllerDataSource , TMBarDataSource {

    
    
    var vc =
        [
        HDStoryboard.shared.child(withIdentifier: "wallVC"),
        HDStoryboard.shared.child(withIdentifier: "videoVC")
        ]
    
    override func viewDidLoad(){
        super.viewDidLoad()

        
        self.dataSource = self
    
        let bar = TMBar.ButtonBar()
        bar.backgroundColor = .clear
        
        bar.layout.transitionStyle = .snap // Customize
        bar.backgroundView.style = .flat(color: .clear)
        bar.backgroundColor = .clear
        bar.tintColor = .clear
        bar.buttons.customize { (button) in
            button.tintColor = .white
            button.selectedTintColor = .white
            button.font = UIFont(name: "Texturina-Bold", size: 16)!
        }
        bar.layout.contentInset = UIEdgeInsets(top: 8, left: 16, bottom: 0, right: 0)
        bar.indicator.cornerStyle = .rounded
        bar.indicator.tintColor = .white
        // Add to view
        addBar(bar, dataSource: self, at: .top)
        // Do any additional setup after loading the view.
    }
    

    
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return vc.count
    }

    func viewController(for pageboyViewController: PageboyViewController,
                        at index: PageboyViewController.PageIndex) -> UIViewController? {
        
        if let tempVC = vc[index] as? WallPaperViewController{
            tempVC.configView()
        }
        if let tempVC = vc[index] as? OldVideoViewController{
            tempVC.fromFav()
        }
        return vc[index]
    }

    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }

    func barItem(for bar: TMBar, at index: Int) -> TMBarItemable {
        var title = ""
        
        if index == 0 {
            title = "Wallpaper"
        }
        if index == 1 {
            title = "Video"
        }
        let item = TMBarItem(title: title)
        return item
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
