//
//  VideoTableViewCell.swift
//  Maradona
//
//  Created by pedram on 11/26/20.
//

import UIKit
import SwiftyJSON
import SwifterSwift

class VideoTableViewCell: UITableViewCell {

    @IBOutlet weak var videoNameLbl: UILabel!
    @IBOutlet weak var videoCoverImg: UIImageView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var favBtn: UIButton!
    
    var selectedImg : UIImage = #imageLiteral(resourceName: "starS")
    var unselectedImg : UIImage = #imageLiteral(resourceName: "star")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        backView.addShadow(ofColor: .black, radius: 16, offset: CGSize(width: 0, height: 8), opacity: 0.1)
        
    }

    var video : Video!{
        didSet{
            do{
                let jsonEncoder = JSONEncoder()
                let jsonData = try jsonEncoder.encode(video)
                let json = String(data: jsonData, encoding: String.Encoding.utf8)
                jsonVideo = String(json!.nsString)
            }catch{
                print(error.localizedDescription)
            }
        }
    }
    
//    func confingCell(){
//        let img = isInFav() ? selectedImg : unselectedImg
////        self.favBtn.setImage(img, for: .normal)
//    }
    
    var jsonVideo : String!
    
//    func isInFav() -> Bool{
//        let fav = UserSettings.favVid
//        print(fav)
//        for item in fav{
//            if item == jsonVideo{
//                return true
//            }
//        }
//        return false
//    }

    @IBAction func favBtnPressed(_ sender: Any) {
//        if isInFav(){
//            var favImages = UserSettings.favVid
//            favImages.removeAll(jsonVideo)
//            UserSettings.favVid = favImages
//        }else{
//            var favImages = UserSettings.favVid
//            favImages.append(jsonVideo)
//            UserSettings.favVid = favImages
//        }
//
//        let img = isInFav() ? selectedImg : unselectedImg
//        self.favBtn.setImage(img, for: .normal)
    }
}
