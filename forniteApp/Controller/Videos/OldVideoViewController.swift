//
//  VideoViewController.swift
//  Maradona
//
//  Created by pedram on 11/26/20.
//

import UIKit
import SwiftyJSON
import NVActivityIndicatorView

class OldVideoViewController: UIViewController , UITableViewDataSource , UITableViewDelegate{
    
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var bottomCons: NSLayoutConstraint!
    
    var videos = [Video](){
        didSet{
            self.tableview.reloadData()
        }
    }
    
    
    
    var cover = [UIImage]()
    var category = ""
    var isFav = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = #colorLiteral(red: 0.03390322626, green: 0.4771031141, blue: 0.9995164275, alpha: 1)
        
        if isFav{
//            self.bottomCons.constant = -24
            let fav = UserSettings.favVid
            videos.removeAll()
            for item in fav{
                let temp = JSON(item)
                do {
                    self.videos.append( try JSONDecoder().decode(Video.self, from: temp.description.data(using: .utf8)!))
                }catch{
                    print(error.localizedDescription)
                }
            }
            self.tableview.reloadData()
            
        }else{
            APIService.getVideoByCategory(category: category) { (vids) in
                self.videos = vids
            }
        }

       
        
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        tableview.backgroundColor = .clear
        // Do any additional setup after loading the view.
    }
    
    func configView(cat : String){
        category = cat
    }
    
    func fromFav(){
        isFav = true
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "videoCell", for: indexPath ) as! VideoTableViewCell
        cell.videoCoverImg.setCoverVideo(id: videos[indexPath.row]._id)
        cell.videoNameLbl.text = videos[indexPath.row].name
        cell.video = videos[indexPath.row]
//        cell.confingCell()
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
        cell.contentView.backgroundColor = .clear
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UserSettings.coins < 2{
            let vc = HDStoryboard.shared.child2(withIdentifier: "coinVC")
            self.present(vc!, animated: true, completion: nil)
            return
        }
        UserSettings.coins -= 2
        NotificationCenter.default.post(name: Notification.Name("coinChange"), object: nil)
        
        let vc = HDStoryboard.shared.child(withIdentifier: "videoPlayerVC") as! VideoPlayerViewController
        vc.video = self.videos[indexPath.row]
        self.present(vc, animated: true, completion: nil)
        
        return;
        guard let url = URL(string: videos[indexPath.row].link) else {
          return //be safe
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}
