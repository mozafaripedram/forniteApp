//
//  VideoViewController.swift
//  Maradona
//
//  Created by pedram on 11/26/20.
//

import UIKit
import SwiftyJSON
//import NVActivityIndicatorView

class VideoViewController: UIViewController , UITableViewDataSource , UITableViewDelegate , UICollectionViewDelegate, UICollectionViewDataSource{
    
    
    
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var bottomCons: NSLayoutConstraint!
    @IBOutlet weak var topMovieLbl: UILabel!
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var topMovieImg: UIImageView!
    
    
    var selected = 0
    
    
    var videos = [Video](){
        didSet{
            print(videos.count)
            self.tableview.reloadData()
        }
    }
    
    var topVideo : Video!{
        didSet{
            self.topMovieImg.setCoverVideo(id: topVideo._id)
            self.topMovieLbl.text = topVideo.name
        }
        
    }
    
    var loadedData : [String : [Video]] = ["hello": [Video]()]
    
    var cats = [String](){
        didSet{
            self.collection.reloadData()
            getVideos()
        }
    }
    
    func getVideos(){
        
        if loadedData.count > 1{
            let m = loadedData["\(selected)"]
            
            if m != nil{
                self.videos = m!
                self.topVideo = m!.randomElement()
                self.tableview.reloadData()
                return
            }
        }
        
        
        APIService.getVideoByCategory(category: cats[selected]) { (vids) in
            self.videos = vids
            self.loadedData["\(self.selected)"] = vids
            self.topVideo = vids.randomElement()
            self.tableview.reloadData()
        }
    }
    
    
    
    var cover = [UIImage]()
    var category = ""
    var isFav = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        APIService.getVideoCategory { (cats) in
            self.cats = cats
        }
        
        
        
        
        
//        if isFav{
//            let fav = UserSettings.favVid
//            videos.removeAll()
//            for item in fav{
//                let temp = JSON(item)
//                do {
//                    self.videos.append( try JSONDecoder().decode(Video.self, from: temp.description.data(using: .utf8)!))
//                }catch{
//                    print(error.localizedDescription)
//                }
//            }
//            self.tableview.reloadData()
//
//        }else{
//            print(category)
//            APIService.getVideoByCategory(category: category) { (vids) in
//                self.videos = vids
//                self.topVideo = vids.randomElement()
//            }
//        }

        collection.delegate = self
        collection.dataSource = self
        collection.backgroundColor = .clear
        
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        tableview.backgroundColor = .clear
        // Do any additional setup after loading the view.
    }
    
    func configView(cat : String){
        category = cat
    }
    
    func fromFav(){
        isFav = true
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cats.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collection.dequeueReusableCell(withReuseIdentifier: "catCell", for: indexPath) as! CategoryCollectionViewCell
        cell.catLbl.text = cats[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == selected {
            cell.isSelected = true
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collection.cellForItem(at: IndexPath(row: selected, section: 0))?.isSelected = false
        selected = indexPath.row
        getVideos()
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "videoCell", for: indexPath ) as! VideoTableViewCell
        cell.videoCoverImg.setCoverVideo(id: videos[indexPath.row]._id)
        cell.videoNameLbl.text = videos[indexPath.row].name
        cell.video = videos[indexPath.row]
//        cell.confingCell()
        cell.backgroundColor = .clear
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
        cell.contentView.backgroundColor = .clear
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = HDStoryboard.shared.child(withIdentifier: "videoPlayerVC") as! VideoPlayerViewController
        vc.video = self.videos[indexPath.row]
        self.present(vc, animated: true, completion: nil)
        return;
        
        guard let url = URL(string: videos[indexPath.row].link) else {
          return //be safe
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    
    
    
    @IBAction func playBtnPressed(_ sender: Any) {
        let vc = HDStoryboard.shared.child(withIdentifier: "playerVC") as! VideoPlayerViewController
        vc.video = topVideo
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
}
