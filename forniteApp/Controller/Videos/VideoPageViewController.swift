//
//  VideoPageViewController.swift
//  forniteApp
//
//  Created by pedram on 12/22/20.
//

import UIKit
import Pageboy
import Tabman

class VideoPageViewController: TabmanViewController, PageboyViewControllerDataSource , TMBarDataSource {

    var vc = [HDStoryboard.shared.child(withIdentifier: "emoteVC")]
    
    var cats = [String](){
        didSet{
            for item in cats{
                let tempVc = HDStoryboard.shared.child(withIdentifier: "videoVC") as! OldVideoViewController
                tempVc.configView(cat: item)
                vc.append(tempVc)
                
            }
            
            self.reloadData()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configBar()
        
        APIService.getVideoCategory { (cats) in
            self.cats = cats
        }
        
        // Do any additional setup after loading the view.
    }
    
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return cats.count + 1
    }

    func viewController(for pageboyViewController: PageboyViewController,
                        at index: PageboyViewController.PageIndex) -> UIViewController? {
        
        if let tempVC = vc[index] as? OldVideoViewController{
//            if index == 0{
//                tempVC.configText(ext: .play)
//            }
//            if index == 1{
//                tempVC.configText(ext: .tip)
//            }
//            if index == 2{
//                tempVC.configText(ext: .level)
//            }
//            if index == 3{
//                tempVC.configText(ext: .car)
//            }
//            if index == 4{
//                tempVC.configText(ext: .ping)
//            }
        }
        
        return vc[index]
    }

    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }

    func barItem(for bar: TMBar, at index: Int) -> TMBarItemable {
        if index == 0 {
            return TMBarItem(title: "Emotes")
        }
        let title = cats[index - 1]
        let item = TMBarItem(title: title)
        return item
    }

    
    
    func configBar(){
        self.dataSource = self
    
        let bar = TMBar.ButtonBar()
        bar.backgroundColor = .clear
        
        bar.layout.transitionStyle = .snap // Customize
        bar.backgroundView.style = .flat(color: .clear)
        bar.backgroundColor = .clear
        bar.tintColor = .clear
        bar.buttons.customize { (button) in
            button.tintColor = .white
            button.selectedTintColor = .white
            button.font = UIFont(name: "Texturina-Bold", size: 16)!
        }
        bar.layout.contentInset = UIEdgeInsets(top: 8, left: 16, bottom: 0, right: 0)
        bar.indicator.cornerStyle = .rounded
        bar.indicator.tintColor = .white
        // Add to view
        addBar(bar, dataSource: self, at: .top)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
