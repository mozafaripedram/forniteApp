//
//  CategoryCollectionViewCell.swift
//  brawl stars guide
//
//  Created by pedram on 11/18/1399 AP.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var catLbl: UILabel!
    
    override var isSelected: Bool {
        didSet{
            
            self.backView.borderColor = .white
            self.backView.borderWidth = isSelected ? 2 : 0
            
        }
    }
}
