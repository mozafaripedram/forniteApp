//
//  ViewController.swift
//  CustomTabBar
//
//  Created by Mehul on 06/12/18.
//  Copyright © 2018 Mehul. All rights reserved.
//

import UIKit
import SwifterSwift
import AVFoundation
import GoogleMobileAds


class TabbarViewController: UIViewController , GADInterstitialDelegate {

    var selectedIndex: Int = 0
    var previousIndex: Int = 0
    
    var viewControllers = [UIViewController]()
    
    @IBOutlet weak var buttonsStack: UIStackView!
    @IBOutlet var buttons:[UIButton]!
    @IBOutlet var tabView:UIView!
    var isShowing = true
    var footerHeight: CGFloat = 60
    
    
    static let firstVC = HDStoryboard.shared.child(withIdentifier: "pageController") 
    static let secondVC = HDStoryboard.shared.child(withIdentifier: "videoPageVC")
    static let thirdVC = HDStoryboard.shared.child(withIdentifier: "navWallVC")
    static let fourthVC = HDStoryboard.shared.child3(withIdentifier: "statsVC")
    static let fifth = HDStoryboard.shared.child(withIdentifier: "settingsVC")
    
    var timer : Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewControllers.append(TabbarViewController.firstVC!)
        viewControllers.append(TabbarViewController.secondVC!)
        viewControllers.append(TabbarViewController.thirdVC!)
        viewControllers.append(TabbarViewController.fourthVC!)
        viewControllers.append(TabbarViewController.fifth!)
        buttons[selectedIndex].isSelected = true
        tabChanged(sender: buttons[selectedIndex])
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideHeader(notification:)), name: Notification.Name("hideTab"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showHeader(notification:)), name: Notification.Name("showTab"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeTab(notification:)), name: Notification.Name("changeTab"), object: nil)
        
        self.view.backgroundColor = .red
        tabView.clipsToBounds = false
//        self.tabView.cornerRadius = 16
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    
    @IBAction func tabChanged(sender:UIButton) {
        previousIndex = selectedIndex
        selectedIndex = sender.tag
        unselectAllImage()
        selectBtn(tag: selectedIndex)
        buttons[previousIndex].isSelected = false
        let previousVC = viewControllers[previousIndex]
        
        previousVC.willMove(toParent: nil)
        previousVC.view.removeFromSuperview()
        previousVC.removeFromParent()
        
        sender.isSelected = true
        
        let vc = viewControllers[selectedIndex]
        vc.view.frame = UIApplication.shared.windows[0].frame
        vc.didMove(toParent: self)
        self.addChild(vc)
        self.view.addSubview(vc.view)
        timer = Timer.scheduledTimer(timeInterval: 50.0, target: self, selector: #selector(self.sayHello), userInfo: nil, repeats: true)
        self.interstitial = createAndLoadInterstitial()
        self.view.bringSubviewToFront(tabView)
       }
    
    
    
    var interstitial: GADInterstitial!
    
    func createAndLoadInterstitial() -> GADInterstitial {
      var interstitial = GADInterstitial(adUnitID: "ca-app-pub-6545436330357450/2129657519")
      interstitial.delegate = self
      interstitial.load(GADRequest())
      return interstitial
    }

    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
    
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
    }

     @objc func sayHello()
     {
//        if self.interstitial.isReady{
//            self.interstitial.present(fromRootViewController: self)
//            interstitial =  createAndLoadInterstitial()
//        }
     }
    
}
    

// MARK: - Actions
extension TabbarViewController {
    
    
    
   
    
    @objc func changeTab(notification: Notification){
        let tag = notification.userInfo!["page"] as! Int
        previousIndex = selectedIndex
        selectedIndex = tag
        
        buttons[previousIndex].isSelected = false
        let previousVC = viewControllers[previousIndex]
        
        previousVC.willMove(toParent: nil)
        previousVC.view.removeFromSuperview()
        previousVC.removeFromParent()
        
        buttons[tag].isSelected = true
        
        let vc = viewControllers[selectedIndex]
        vc.view.frame = UIApplication.shared.windows[0].frame
        vc.didMove(toParent: self)
        self.addChild(vc)
        self.view.addSubview(vc.view)
        
        self.view.bringSubviewToFront(tabView)
    }
    
    @objc func hideHeader(notification: Notification) {
        isShowing = false
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
            self.tabView.frame = CGRect(x: self.tabView.frame.origin.x, y: (self.view.frame.height + self.view.safeAreaInsets.bottom + 16), width: self.tabView.frame.width, height: self.footerHeight)
        })
    }
    
    @objc func showHeader(notification: Notification) {
        
        if !isShowing{
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
                self.tabView.frame = CGRect(x: self.tabView.frame.origin.x, y: self.view.frame.height - (self.footerHeight + self.view.safeAreaInsets.bottom + 16), width: self.tabView.frame.width, height: self.footerHeight)
                self.isShowing = true
            })
        }
        
    }
    
    
    func unselectAllImage(){
        for item in buttonsStack.arrangedSubviews{
            (item as! UIButton).setImage(UIImage(named: "\(item.tag)"), for: .normal)
        }
    }
    
    func selectBtn(tag : Int){
        let item = buttonsStack.arrangedSubviews[tag]
        UIView.transition(with: item, duration: 0.3, options: .transitionFlipFromLeft, animations: {
            (item as! UIButton).setImage(UIImage(named: "\(item.tag)s"), for: .normal)
               }, completion: nil)
    }
}

