//
//  LoginViewController.swift
//  forniteApp
//
//  Created by pedram on 1/13/21.
//

import UIKit

class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var winsLbl: UILabel!
    @IBOutlet weak var matchesLbl: UILabel!
    @IBOutlet weak var killsLbl: UILabel!
    @IBOutlet weak var proImg: UIImageView!
    
    var info : Info!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nameLbl.text = UserSettings.userName
        self.winsLbl.text = "\(info.wins)"
        self.matchesLbl.text = "\(info.matches)"
        self.killsLbl.text = "\(info.kills)"
        var imgName = "pro\(Int.random(in: 1...15))"
        self.proImg.image = UIImage(named: imgName )
        UserSettings.userImg = imgName
    }
    
    
    func configView(info : Info){
        self.info = info
    }

    @IBAction func loginBtnPressed(_ sender: Any) {
        let vc = HDStoryboard.shared.child(withIdentifier: "tabbarViewController")
        vc?.modalPresentationStyle = .fullScreen
        UserSettings.isLogIn = true
        self.present(vc!, animated: true, completion: nil)
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
