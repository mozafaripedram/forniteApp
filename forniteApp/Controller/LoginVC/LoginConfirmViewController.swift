//
//  LoginConfirmViewController.swift
//  forniteApp
//
//  Created by pedram on 1/13/21.
//

import UIKit

class LoginConfirmViewController: UIViewController {
    
    @IBOutlet weak var usernameTxt: UITextField!
    
    @IBOutlet weak var bottomCons: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)

        // Do any additional setup after loading the view.
    }
    
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            self.bottomCons.constant = keyboardHeight - 60
        }
    }

    @objc func keyboardWillDisappear() {
        self.bottomCons.constant = 0
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func continueBtnPressed(_ sender: Any) {
        let name = self.usernameTxt.text
        if name == ""{
            return
        }
        APIService.getStat(name: name!) { (status, info) in
            if status == 200{
                UserSettings.userName = name!
                let vc = HDStoryboard.shared.child2(withIdentifier: "confirmVC") as! LoginViewController
                vc.configView(info: info!)
                vc.modalPresentationStyle = .overFullScreen
                self.present(vc, animated: true, completion: nil)
            }else{
                self.showAlert(title: "Username Not valid", message: "Can't find a acount with entered username.")
            }
        }
    }
    
    
    @IBAction func noAccountBtnPressed(_ sender: Any) {
        let vc = HDStoryboard.shared.child(withIdentifier: "tabbarViewController")
        vc?.modalPresentationStyle = .fullScreen
//        UserSettings.isLogIn = true
        self.present(vc!, animated: true, completion: nil)
    }
    
    
}
