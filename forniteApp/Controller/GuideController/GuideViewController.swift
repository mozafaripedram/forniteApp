//
//  GuideViewController.swift
//  Among Us
//
//  Created by pedram on 11/23/20.
//

import UIKit

class GuideViewController: UIViewController {
    
    
    @IBOutlet weak var guideTxt: UITextView!
    
    let availableLanguages = ["Arabic" , "English" , "French" , "German", "japanese" , "Portuguese", "Russian" , "Spanish" , "Chinese"]
    let ekhtesar: [lang] = [ .ar , .en ,.fr ,.gr ,.ja ,.pr ,.ru ,.sp , .ch ]
    var actionSheet: UIAlertController!
    
    var ext : ext!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        changeBackGorundColor()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupView()
    }
    
    func setupView(){
        if UserSettings.getLang() == .ar{
            self.guideTxt.textAlignment = .right
        }else{
            self.guideTxt.textAlignment = .left
        }
        self.guideTxt.text = FileReader.shared.getText(lang: UserSettings.getLang(), ext: ext)
    }
    
    @IBAction func languageBtnPressed(_ sender: Any) {
        actionSheet = UIAlertController(title: nil, message: "Switch Guide Language", preferredStyle: UIAlertController.Style.actionSheet)
        for language in 0...availableLanguages.count - 1{
            let displayName = availableLanguages[language]
            let languageAction = UIAlertAction(title: displayName, style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                UserSettings.setLang(lang: self.ekhtesar[language])
                self.setupView()
//                self.text.text = FileReader.shared.getText(lang: self.ekhtesar[language])
                
            })
            actionSheet.addAction(languageAction)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {
            (alert: UIAlertAction) -> Void in
        })
        actionSheet.addAction(cancelAction)
        if let popoverController = actionSheet.popoverPresentationController {
            popoverController.sourceView = self.view //to set the source of your alert
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
            popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
        }
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    func configText(ext : ext){
        self.ext = ext
    }
    

}
