//
//  GuideFirstViewController.swift
//  PubgApp
//
//  Created by pedram on 12/23/1399 AP.
//

import UIKit

class GuideFirstViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource {

    @IBOutlet weak var collection: UICollectionView!
    
    var names = ["How to play", "20 Tips" , "Ping" ,"How to level up fast" , "How To Start Play" , "Cars"]
    var images : [UIImage?] = [UIImage(named: "NoPath - Copy (8)") , UIImage(named: "NoPath - Copy (9)") , UIImage(named: "NoPath - Copy (10)") , UIImage(named: "NoPath - Copy (11)"), UIImage(named: "NoPath - Copy (12)")]
    var colors : [UIColor] = [#colorLiteral(red: 0.101359047, green: 0.5450540185, blue: 0.5531491637, alpha: 1) , #colorLiteral(red: 0.6525781751, green: 0.1259818375, blue: 0.1012633517, alpha: 1) , #colorLiteral(red: 0.9196354151, green: 0.6044333577, blue: 0.1007115021, alpha: 1) , #colorLiteral(red: 0.08372496814, green: 0.2116751373, blue: 0.2604709566, alpha: 1) , #colorLiteral(red: 0.8542361259, green: 0.8235366344, blue: 0.8478928804, alpha: 1)]
    
    var guides : [ext] = [.play ,.tip ,.ping ,.level , .car]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: self.collection.width - 64, height: self.collection.height - 64)
        layout.scrollDirection = .horizontal
        
        collection.collectionViewLayout = layout
        collection.dataSource = self
        collection.delegate = self
//        collection.isPagingEnabled = true
        
        
        collection.backgroundColor = .clear
        collection.clipsToBounds = false
//        collection.ind

        // Do any additional setup after loading the view.
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collection.dequeueReusableCell(withReuseIdentifier: "guideCell", for: indexPath) as! GuideCollectionViewCell
        cell.coverImg.image = self.images[indexPath.row]
        cell.nameLbl.text = self.names[indexPath.row]
        cell.backgroundColor = .clear
        cell.coverImg.backgroundColor = colors[indexPath.row]
        cell.backView.addShadow(ofColor: .white, radius: 8, offset: CGSize(), opacity: 0.2)
        cell.clipsToBounds = false
        cell.contentView.clipsToBounds = false
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = HDStoryboard.shared.child(withIdentifier: "GuideVC") as! GuideViewController
        vc.ext = self.guides[indexPath.row]
        self.present(vc, animated: true, completion: nil)
    }
    

}
