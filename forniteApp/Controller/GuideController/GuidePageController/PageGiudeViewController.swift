//
//  PageGiudeViewController.swift
//  Among Us
//
//  Created by pedram on 12/4/20.
//

import UIKit
import Pageboy
import Tabman

class PageGiudeViewController: TabmanViewController, PageboyViewControllerDataSource , TMBarDataSource {
    
    var vc =
        [
        HDStoryboard.shared.child(withIdentifier: "GuideVC"),
        HDStoryboard.shared.child(withIdentifier: "GuideVC"),
        HDStoryboard.shared.child(withIdentifier: "GuideVC"),
        HDStoryboard.shared.child(withIdentifier: "GuideVC"),
        HDStoryboard.shared.child(withIdentifier: "GuideVC")
        ]
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.dataSource = self
        
        
    
        let bar = TMBar.ButtonBar()
        bar.backgroundColor = .clear
        
        bar.layout.transitionStyle = .snap // Customize
        bar.backgroundView.style = .flat(color: .clear)
        bar.backgroundColor = .clear
        bar.tintColor = .clear
        bar.buttons.customize { (button) in
            button.tintColor = .white
            button.selectedTintColor = .white
            button.font = UIFont(name: "Texturina-Bold", size: 16)!
            
        }
        bar.layout.contentInset = UIEdgeInsets(top: 8, left: 16, bottom: 0, right: 0)
        bar.indicator.cornerStyle = .rounded
        bar.indicator.tintColor = .white
        // Add to view
        addBar(bar, dataSource: self, at: .top)
        
    }
    
    
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return 5
    }

    func viewController(for pageboyViewController: PageboyViewController,
                        at index: PageboyViewController.PageIndex) -> UIViewController? {
        
        if let tempVC = vc[index] as? GuideViewController{
            if index == 0{
                tempVC.configText(ext: .play)
            }
            if index == 1{
                tempVC.configText(ext: .tip)
            }
            if index == 2{
                tempVC.configText(ext: .level)
            }
            if index == 3{
                tempVC.configText(ext: .car)
            }
            if index == 4{
                tempVC.configText(ext: .ping)
            }
        }
        
        return vc[index]
    }

    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }

    func barItem(for bar: TMBar, at index: Int) -> TMBarItemable {
        var title = ""
        
        if index == 0 {
            title = "How to play"
        }
        if index == 1 {
            title = "20 Tips"
        }
        if index == 2 {
            title = "Level up"
        }
        if index == 3{
            title = "Cars"
        }
        if index == 4 {
            title = "Ping"
        }
        let item = TMBarItem(title: title)
        return item
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
