//
//  CircularTableViewCell.swift
//  forniteApp
//
//  Created by pedram on 11/5/1399 AP.
//

import UIKit
import CircleProgressBar
import QuartzCore

class CircularTableViewCell: UITableViewCell {

    @IBOutlet weak var circularProgress: CircleProgressBar!
    @IBOutlet weak var friendNameLbl: UILabel!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    
    var UserInfo : Info!
    var friendInfo : Info!
    var friendName = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        circularProgress.progressBarProgressColor = .red
        circularProgress.progressBarTrackColor = .yellow
        circularProgress.backgroundColor = .clear
        circularProgress.hintHidden = true
        circularProgress.progressBarWidth = 15
        
    }

    
    func configCell(userInfo : Info , friendInfo : Info , title : String){
        self.UserInfo = userInfo
        self.friendInfo = friendInfo
        
        self.titleLbl.text = title
        var progres = 0.0
        if title == "Score"{
            progres = Double(Double(UserInfo.score) / Double(friendInfo.score + userInfo.score))
            friendNameLbl.text = "\(friendInfo.score)"
            usernameLbl.text = "\(userInfo.score)"
            circularProgress.setProgress(CGFloat(progres), animated: true)
        }
        if title == "K/D"{
            progres = (Double(UserInfo.kd) / Double(friendInfo.kd + userInfo.kd))
            friendNameLbl.text = "\(friendInfo.kd)"
            usernameLbl.text = "\(userInfo.kd)"
            circularProgress.setProgress(CGFloat(progres), animated: true)
        }
        print(progres)
        print(CGFloat(progres))
        
        
        
//        circularProgress.setProgress(CGFloat(progres), animated: true)
    }

}
