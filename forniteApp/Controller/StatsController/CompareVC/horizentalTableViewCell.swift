//
//  horizentalTableViewCell.swift
//  forniteApp
//
//  Created by pedram on 11/3/1399 AP.
//

import UIKit

class horizentalTableViewCell: UITableViewCell , UICollectionViewDelegate , UICollectionViewDataSource {
    
    
    @IBOutlet weak var collection: UICollectionView!
    
    var info : Info!
    var friendStat : Info!
    
    var titles = ["Matches" , "Wins" , "Kills"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        do{
            info = try JSONDecoder().decode(Info.self, from: UserSettings.userInfo.data(using: .utf8)!)
        }catch{
            print(error.localizedDescription)
        }
        collection.backgroundColor = #colorLiteral(red: 0.01547858771, green: 0.1839128733, blue: 0.360604316, alpha: 1)
        self.contentView.backgroundColor = .clear
        self.backgroundColor = .clear
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: HDUtils.width() / 4, height: 280)
        layout.scrollDirection = .horizontal
//        layout
        collection.dataSource = self
        collection.delegate = self
        collection.collectionViewLayout = layout
    }
    
    func configCell(friendInfo : Info){
        
        self.friendStat = friendInfo
        
        print(info.wins)
        print(friendStat.wins)
        
        self.collection.reloadData()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collection.dequeueReusableCell(withReuseIdentifier: "mileiiCell", for: indexPath) as! MileiiCollectionViewCell
        
        cell.configCell(userInfo: info, friendInfo: friendStat, titleName: titles[indexPath.row])
        return cell
    }
    
}
