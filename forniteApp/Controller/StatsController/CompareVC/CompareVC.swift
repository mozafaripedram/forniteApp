//
//  CompareVC.swift
//  forniteApp
//
//  Created by pedram on 11/3/1399 AP.
//

import UIKit

class CompareVC: UIViewController , UITableViewDelegate , UITableViewDataSource {
   
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var friendsLbl: UILabel!
    
    
    var friendInfo  : Info!
    var info : Info!
    
    @IBOutlet weak var tableview: UITableView!
    
    var friendName = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        APIService.getStat(name: friendName ,isFriend: true) { (status, info) in
            if status == 200{
                self.friendInfo = info
                self.tableview.reloadData()
            }
        }
        do{
            info = try JSONDecoder().decode(Info.self, from: UserSettings.userInfo.data(using: .utf8)!)
        }catch{
            print(error.localizedDescription)
        }
        
        self.userName.text = UserSettings.userName
        self.friendsLbl.text = friendName
        
        tableview.separatorStyle = .none
        tableview.backgroundColor = .clear
        tableview.delegate = self
        tableview.dataSource = self
        
    }
    
    
    @IBAction func closeBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if friendInfo == nil{
            return 0
        }
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 1{
            let cell = tableview.dequeueReusableCell(withIdentifier: "circelCell", for: indexPath) as! CircularTableViewCell
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            cell.configCell(userInfo: info, friendInfo: friendInfo, title: "Score")
            cell.selectionStyle = .none
            return cell
        }
        if indexPath.row == 2{
            let cell = tableview.dequeueReusableCell(withIdentifier: "circelCell", for: indexPath) as! CircularTableViewCell
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            cell.configCell(userInfo: info, friendInfo: friendInfo, title: "K/D")
            cell.selectionStyle = .none
            return cell
        }
        
        let cell = tableview.dequeueReusableCell(withIdentifier: "hCell", for: indexPath) as! horizentalTableViewCell
        cell.configCell(friendInfo: friendInfo)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
           return 350
        }
        return 300
    }

}
