//
//  MileiiCollectionViewCell.swift
//  forniteApp
//
//  Created by pedram on 11/3/1399 AP.
//

import UIKit

class MileiiCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var friendStatHeight: NSLayoutConstraint!
    @IBOutlet weak var myStatHeight: NSLayoutConstraint!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var friendScoreLbl: UILabel!
    @IBOutlet weak var myScoreLbl: UILabel!
    
    
    func configCell(userInfo : Info , friendInfo : Info , titleName : String){
        
        self.titleLbl.text = titleName
        

        print(userInfo.wins)
        print(friendInfo.wins)
        
        switch titleName {
        case "Matches":
            self.friendScoreLbl.text = "\(friendInfo.matches)"
            self.myScoreLbl.text = "\(userInfo.matches)"
            
            self.friendStatHeight.constant = calcHeight(number: CGFloat(friendInfo.matches))
            self.myStatHeight.constant = calcHeight(number: CGFloat(userInfo.matches))
        case "Wins":
            self.friendScoreLbl.text = "\(friendInfo.wins)"
            self.myScoreLbl.text = "\(userInfo.wins)"
            
            self.friendStatHeight.constant = calcHeight(number: CGFloat(friendInfo.wins))
            self.myStatHeight.constant = calcHeight(number: CGFloat(userInfo.wins))
            
            
        case "Kills":
            self.friendScoreLbl.text = "\(friendInfo.kills)"
            self.myScoreLbl.text = "\(userInfo.kills)"
            
            self.friendStatHeight.constant = calcHeight(number: CGFloat(friendInfo.kills))
            self.myStatHeight.constant = calcHeight(number: CGFloat(userInfo.kills))
            
            
        default:
            self.friendScoreLbl.text = "\(friendInfo.kills)"
            self.myScoreLbl.text = "\(userInfo.kills)"
            
            self.friendStatHeight.constant = calcHeight(number: CGFloat(friendInfo.matches))
            self.myStatHeight.constant = calcHeight(number: CGFloat(userInfo.matches))
        }
        
        
        
        
    }
    
    func calcHeight(number : CGFloat) -> CGFloat {
        
        var h: CGFloat = 30
        h  = CGFloat(log2(number) / 20 * 250)
        
        if h < 30 {
            h = 30
        }else if h > 200{
            h = 200
        }
        return h
    }
    
    
    
}
