//
//  YouVersusViewController.swift
//  forniteApp
//
//  Created by pedram on 10/27/1399 AP.
//

import UIKit
import SwiftyJSON

class YouVersusViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    
    
    @IBOutlet weak var aboutYouBtn: UIButton!
    @IBOutlet weak var friendsBtn: UIButton!
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var wizhwizh: UIActivityIndicatorView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var signLbl: UILabel!
    
    var info : Info!
    
    var isAboutYou = true

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
        if UserSettings.isLogIn {
            self.signInBtn.isHidden = true
            self.signLbl.isHidden = true
            self.stopAnimateWizhwizh()
            
            do{
                info = try JSONDecoder().decode(Info.self, from: UserSettings.userInfo.data(using: .utf8)!)
            }catch{
                print(error.localizedDescription)
            }
            let layout = FlowLayout()
            layout.itemSize = CGSize(width: HDUtils.width(), height: collection.height)
            layout.scrollDirection = .vertical
            
            collection.collectionViewLayout = layout
            collection.backgroundColor = .clear
            collection.isPagingEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
            view.addGestureRecognizer(tap)
            
            
            collection.register(UINib(nibName: "AboutYouCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "aboutCell")
            collection.register(UINib(nibName: "FriendsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "friendColCell")
            
            collection.delegate = self
            collection.dataSource = self
        }else{
            self.signInBtn.isHidden = false
            self.signLbl.isHidden = false
            self.animateWizhwizh()
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        self.view.endEditing(true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if isAboutYou{
            let cell = collection.dequeueReusableCell(withReuseIdentifier: "aboutCell", for: indexPath) as! AboutYouCollectionViewCell
            cell.configCell(info: info)
            return cell
        }else{
            let cell = collection.dequeueReusableCell(withReuseIdentifier: "friendColCell", for: indexPath) as! FriendsCollectionViewCell
            cell.parent = self
            return cell
        }
    }
    
    
    func animateWizhwizh(){
        blurView.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.3) {
            self.blurView.alpha = 1
            self.wizhwizh.startAnimating()
        }
    }
    
    func stopAnimateWizhwizh(){
        blurView.isUserInteractionEnabled = true
        UIView.animate(withDuration: 0.3) {
            self.blurView.alpha = 0
            self.wizhwizh.stopAnimating()
        }
    }
    
    @IBAction func signInBtnPressed(_ sender: Any) {
        let vc = HDStoryboard.shared.child2(withIdentifier: "loginVC")
        vc?.modalPresentationStyle = .fullScreen
        self.present(vc!, animated: true, completion: nil)
    }
    
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
    
    @IBAction func aboutYouBtnPressed(_ sender: Any) {
        self.isAboutYou = true
        deselectAll()
        self.aboutYouBtn.backgroundColor = #colorLiteral(red: 0.01695790887, green: 0.1880499721, blue: 0.2813356817, alpha: 1)
        self.aboutYouBtn.titleColorForNormal = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    
    @IBAction func friendBtnPressed(_ sender: Any) {
        self.isAboutYou = false
        deselectAll()
        self.friendsBtn.backgroundColor = #colorLiteral(red: 0.01695790887, green: 0.1880499721, blue: 0.2813356817, alpha: 1)
        self.friendsBtn.titleColorForNormal = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    
    func deselectAll(){
        collection.reloadData()
        self.aboutYouBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.aboutYouBtn.titleColorForNormal = #colorLiteral(red: 0.01695790887, green: 0.1880499721, blue: 0.2813356817, alpha: 1)
        self.friendsBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.friendsBtn.titleColorForNormal = #colorLiteral(red: 0.01695790887, green: 0.1880499721, blue: 0.2813356817, alpha: 1)
       
    }
    

    
}
