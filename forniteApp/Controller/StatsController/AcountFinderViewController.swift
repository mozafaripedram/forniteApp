//
//  AcountFinderViewController.swift
//  forniteApp
//
//  Created by pedram on 1/8/21.
//

import UIKit

class AcountFinderViewController: UIViewController , UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        searchBar.setImage(UIImage(), for: .search, state: .normal)
        searchBar.searchTextField.clearButtonMode = .never
        searchBar.tintColor = .clear
        searchBar.searchTextField.tintColor = .black
        searchBar.searchBarStyle = .prominent
        
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        
        textFieldInsideSearchBar?.textColor = .white
        textFieldInsideSearchBar?.backgroundColor = .white
        searchBar.searchTextField.cornerRadius = 0
        searchBar.searchTextField.textColor = .black
        searchBar.textField?.backgroundColor = .white
        searchBar.searchTextField.font = UIFont(name: "Burbank Big Condensed Black", size: 25)!
        // Do any additional setup after loading the view.
    }
    

    
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        let name = searchBar.text
//        
//        if name == "" {
//            return
//        }
//        APIService.getStat(name: name!) { (status, info) in
//            if status != 200{
//                let alert = UIAlertController(title: "No username found")
//                alert.show()
//                
//            }else{
//                print(info)
//            }
//        }
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let name = searchBar.text
        if name == "" {
            return
        }
        APIService.getStat(name: name!) { (status, info) in
            print("hi")
            if status != 200{
                let alert = UIAlertController(title: "No username found")
                alert.show()
                
            }else{
                print(info)
            }
        }
    }
    

}
