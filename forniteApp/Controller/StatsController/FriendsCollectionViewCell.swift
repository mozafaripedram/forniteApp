//
//  FriendsCollectionViewCell.swift
//  forniteApp
//
//  Created by pedram on 10/28/1399 AP.
//

import UIKit

class FriendsCollectionViewCell: UICollectionViewCell , UITableViewDelegate , UITableViewDataSource ,  UISearchBarDelegate  {
    
    

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet weak var allBtn: UIButton!
    
    
    var parent : YouVersusViewController!
    var favFriends = UserSettings.favFriendsID
    var names = UserSettings.friendsID
    var isFav = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        searchBar.delegate = self
        searchBar.setImage(UIImage(), for: .search, state: .normal)
        searchBar.searchTextField.clearButtonMode = .never
        searchBar.tintColor = .clear
        searchBar.searchTextField.tintColor = .black
        searchBar.searchBarStyle = .prominent
        
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        
        textFieldInsideSearchBar?.textColor = .white
        textFieldInsideSearchBar?.backgroundColor = .white
        searchBar.searchTextField.cornerRadius = 0
        searchBar.searchTextField.textColor = .black
        searchBar.textField?.backgroundColor = .white
        searchBar.searchTextField.font = UIFont(name: "Burbank Big Condensed Black", size: 25)!
        
        tableview.delegate = self
        tableview.dataSource = self
        tableview.backgroundColor = .clear
        tableview.separatorStyle = .none
        tableview.register(UINib(nibName: "FriendsTableViewCell", bundle: nil), forCellReuseIdentifier: "friendNameCell")
    }
    
    
    func relaodTableData(){
        self.names = UserSettings.friendsID
        self.favFriends = UserSettings.favFriendsID
        self.tableview.reloadData()
    }
    
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let name = searchBar.text
        if name == ""{
            let alert = UIAlertController(title: "Please enter a username")
            self.parent.present(alert, animated: true, completion: nil)
            return
        }
        self.parent.animateWizhwizh()
        APIService.getStat(name: name!) { (status, info) in
            if status == 200{
                //user founded
                let alertController = UIAlertController(title: "User founded", message: "Do you want to add \(name!) to your friends", preferredStyle: .alert)
                
                // Create the actions
                let okAction = UIAlertAction(title: "Yes", style: .default) {
                    UIAlertAction in
                    UserSettings.friendsID.append(name!)
                    self.relaodTableData()
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
                    UIAlertAction in
                    
                }
                // Add the actions
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                
                // Present the controller
                self.parent.present(alertController, animated: true, completion: nil)
                self.parent.stopAnimateWizhwizh()
            }
            else{
                let alert = UIAlertController(title: "No Account founded")
                self.parent.present(alert, animated: true, completion: nil)
                self.parent.stopAnimateWizhwizh()
                return
            }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFav{
            return favFriends.count
        }
        return names.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "friendNameCell", for: indexPath) as! FriendsTableViewCell
        cell.nameLbl.text = isFav ? favFriends[indexPath.row] : names[indexPath.row]
        cell.isLiked = favFriends.contains(isFav ? favFriends[indexPath.row] : names[indexPath.row])
        cell.setLiked()
        cell.parent = self
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = HDStoryboard.shared.child3(withIdentifier: "compareVC") as! CompareVC
        vc.friendName = isFav ? favFriends[indexPath.row] : names[indexPath.row]
        self.parent.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    @IBAction func favBtnPressed(_ sender: Any) {
        self.isFav = true
        deSelectAll()
        self.favBtn.setTitleColor(.white, for: .normal)
        self.favBtn.backgroundColor = #colorLiteral(red: 0.01695790887, green: 0.1880499721, blue: 0.2813356817, alpha: 1)
        self.tableview.reloadData()
    }
    
    @IBAction func allBtnPressed(_ sender: Any) {
        self.isFav = false
        deSelectAll()
        self.allBtn.setTitleColor(.white, for: .normal)
        self.allBtn.backgroundColor = #colorLiteral(red: 0.01695790887, green: 0.1880499721, blue: 0.2813356817, alpha: 1)
        self.tableview.reloadData()
    }
    
    func deSelectAll(){
        self.allBtn.setTitleColor(#colorLiteral(red: 0.01695790887, green: 0.1880499721, blue: 0.2813356817, alpha: 1), for: .normal)
        self.favBtn.setTitleColor(#colorLiteral(red: 0.01695790887, green: 0.1880499721, blue: 0.2813356817, alpha: 1), for: .normal)
        self.allBtn.backgroundColor = .white
        self.favBtn.backgroundColor = .white
    }
    
    
}
