//
//  FriendsTableViewCell.swift
//  forniteApp
//
//  Created by pedram on 10/28/1399 AP.
//

import UIKit

class FriendsTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    
    var likedImg : UIImage = #imageLiteral(resourceName: "Icon material-favorite")
    var unLikeImg : UIImage = #imageLiteral(resourceName: "Icon material-favorite-border")
    
    var isLiked = false
    var parent : FriendsCollectionViewCell!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    func setLiked(){
        if isLiked{
            self.likeBtn.setImage(likedImg, for: .normal)
        }else{
            self.likeBtn.setImage(unLikeImg, for: .normal)
        }
    }
    @IBAction func likeBtnPressed(_ sender: Any) {
        
        if isLiked{
            self.likeBtn.setImage(likedImg, for: .normal)
            UserSettings.favFriendsID.removeAll(nameLbl.text!)
            
        }else{
            self.likeBtn.setImage(unLikeImg, for: .normal)
            UserSettings.favFriendsID.removeAll(nameLbl.text!)
            UserSettings.favFriendsID.append(nameLbl.text!)
        }
        isLiked = !isLiked
        self.parent.relaodTableData()
    }
    @IBAction func deleteBtnPressed(_ sender: Any) {
        let name = nameLbl.text
        let alertController = UIAlertController(title: "Are you sure", message: "Do you want to remove \(name!) from your friends", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: .default) {
            UIAlertAction in
            UserSettings.friendsID.removeAll(name!)
            UserSettings.favFriendsID.removeAll(name!)
            self.parent.relaodTableData()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
            UIAlertAction in
            
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.parent.parent.present(alertController, animated: true, completion: nil)
        
    }
    
}
