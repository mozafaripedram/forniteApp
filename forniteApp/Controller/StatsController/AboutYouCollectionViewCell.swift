//
//  AboutYouCollectionViewCell.swift
//  forniteApp
//
//  Created by pedram on 10/28/1399 AP.
//

import UIKit

class AboutYouCollectionViewCell: UICollectionViewCell {
    
    //top views outlets
    @IBOutlet weak var matchesLb: UILabel!
    @IBOutlet weak var winsLbl: UILabel!
    @IBOutlet weak var winsPercent: UILabel!
    @IBOutlet weak var killsLbl: UILabel!
    
    
    // downStack outlets
    @IBOutlet weak var scoreLbl: UILabel!
    @IBOutlet weak var kdLbl: UILabel!
    @IBOutlet weak var killsPerMinLbl: UILabel!
    @IBOutlet weak var killsPerMatch: UILabel!
    @IBOutlet weak var top6Lbl: UILabel!
    @IBOutlet weak var top3Lbl: UILabel!
    @IBOutlet weak var timePlayedLbl: UILabel!
    
    
    
    
    func configCell(info : Info){
        //top view
        self.matchesLb.text = "\(info.matches)"
        self.winsLbl.text = "\(info.wins)"
        self.winsPercent.text = "\(info.winRate)"
        self.killsLbl.text = "\(info.kills)"
        
        //downStack
        self.scoreLbl.text = "\(info.score)"
        self.kdLbl.text = "\(info.kd)"
        self.killsPerMinLbl.text = "\(info.killsPerMin)"
        self.killsPerMatch.text = "\(info.killsPerMatch)"
        self.top6Lbl.text = "\(info.top6 + info.top12 + info.top25)"
        self.top3Lbl.text = "\(info.top3 + info.top5 + info.top10)"
        self.timePlayedLbl.text = "\(info.minutesPlayed) min"
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    


}
