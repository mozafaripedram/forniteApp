//
//  ShopViewController.swift
//  forniteApp
//
//  Created by pedram on 10/27/1399 AP.
//

import UIKit
import SwiftyStoreKit

class ShopViewController: UIViewController , UITableViewDelegate , UITableViewDataSource{
    
    @IBOutlet weak var tableview: UITableView!
    var price = ["2,99$", "4,99$" , "7,99$"]
    var identifires = ["fortnite.20", "fortnite.50" ,"fortnite.100"]
    var amount = [20,50,100]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.delegate = self
        self.tableview.dataSource = self
        SwiftyStoreKit.retrieveProductsInfo(["fortnite.20"]) { result in
            if let product = result.retrievedProducts.first {
                let priceString = product.localizedPrice!
                self.price[0] = priceString
                print("Product: \(product.localizedDescription), price: \(priceString)")
            }
            else if let invalidProductId = result.invalidProductIDs.first {
                print("Invalid product identifier: \(invalidProductId)")
            }
            else {
                print("Error: \(result.error)")
            }
        }
        SwiftyStoreKit.retrieveProductsInfo(["fortnite.50"]) { result in
            if let product = result.retrievedProducts.first {
                let priceString = product.localizedPrice!
                self.price[1] = priceString
                print("Product: \(product.localizedDescription), price: \(priceString)")
            }
            else if let invalidProductId = result.invalidProductIDs.first {
                print("Invalid product identifier: \(invalidProductId)")
            }
            else {
                print("Error: \(result.error)")
            }
        }
        SwiftyStoreKit.retrieveProductsInfo(["fortnite.100"]) { result in
            if let product = result.retrievedProducts.first {
                let priceString = product.localizedPrice!
                self.price[2] = priceString
                self.tableview.reloadData()
                print("Product: \(product.localizedDescription), price: \(priceString)")
            }
            else if let invalidProductId = result.invalidProductIDs.first {
                print("Invalid product identifier: \(invalidProductId)")
            }
            else {
                print("Error: \(result.error)")
            }
        }
        
        self.tableview.backgroundColor = #colorLiteral(red: 0.004554671235, green: 0.1173931584, blue: 0.230671823, alpha: 1)
        tableview.separatorStyle = .none
        tableview.contentInset = UIEdgeInsets(top: 32, left: 0, bottom: 0, right: 0)
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "priceCell", for: indexPath) as! ShopCell
        cell.coinLbl.text =  "\(amount[indexPath.row])"
        cell.priceLbl.text = "\(price[indexPath.row])"
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SwiftyStoreKit.purchaseProduct(self.identifires[indexPath.row], quantity: 1, atomically: true) { result in
            switch result {
            case .success(let purchase):
                print("Purchase Success: \(purchase.productId)")
                UserSettings.coins += self.amount[indexPath.row]
                NotificationCenter.default.post(name: Notification.Name("coinChange"), object: nil)
            case .error(let error):
                switch error.code {
                case .unknown: print("Unknown error. Please contact support")
                case .clientInvalid: print("Not allowed to make the payment")
                case .paymentCancelled: break
                case .paymentInvalid: print("The purchase identifier was invalid")
                case .paymentNotAllowed: print("The device is not allowed to make the payment")
                case .storeProductNotAvailable: print("The product is not available in the current storefront")
                case .cloudServicePermissionDenied: print("Access to cloud service information is not allowed")
                case .cloudServiceNetworkConnectionFailed: print("Could not connect to the network")
                case .cloudServiceRevoked: print("User has revoked permission to use this cloud service")
                default: print((error as NSError).localizedDescription)
                }
            }
        }
        
    }

    @IBAction func privacyBtnPressed(_ sender: Any) {
        
    }
}
