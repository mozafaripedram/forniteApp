//
//  EmoteViewController.swift
//  forniteApp
//
//  Created by pedram on 12/23/20.
//

import UIKit
import TagCellLayout
import SHSearchBar

class EmoteViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout , UISearchBarDelegate , TagCellLayoutDelegate{
    
    
    
    func tagCellLayoutTagSize(layout: TagCellLayout, atIndex index: Int) -> CGSize {
        return CGSize(width: emotes[index].name.size(withAttributes:[.font: UIFont(name: "Burbank Big Condensed Black", size: 25)!]).width + 32, height: 56)
    }
    
    
    var l1 : TagCellLayout!
    
    var emotes = [Emote]()
    var tempEmotes = [Emote]()
    var shouldUseTemp = false
    
    
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        APIService.getEmotes { (emotes) in
            self.emotes = emotes
            self.collection.reloadData()
        }
        
        
        searchBar.delegate = self
        searchBar.setImage(UIImage(), for: .search, state: .normal)
        searchBar.searchTextField.clearButtonMode = .never
        searchBar.tintColor = .clear
        searchBar.searchTextField.tintColor = .black
        searchBar.searchBarStyle = .prominent
        
//        searchBar.delegate =de
        //        searchBar.backgroundImage = UIImage()
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        
        textFieldInsideSearchBar?.textColor = .white
        textFieldInsideSearchBar?.backgroundColor = .white
        searchBar.searchTextField.cornerRadius = 0
        searchBar.searchTextField.textColor = .black
        searchBar.textField?.backgroundColor = .white
        searchBar.searchTextField.font = UIFont(name: "Burbank Big Condensed Black", size: 25)!
        
        let layout = UICollectionViewFlowLayout()
        l1 = TagCellLayout(alignment: .center, delegate: self)
        
        collection.collectionViewLayout = l1
        
        collection.delegate = self
        collection.dataSource = self
        collection.backgroundColor = .clear
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return shouldUseTemp ? tempEmotes.count : emotes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collection.dequeueReusableCell(withReuseIdentifier: "emoteCell", for: indexPath) as! EmoteCollectionViewCell
        cell.emoteNameLbl.text = shouldUseTemp ? tempEmotes[indexPath.row].name : emotes[indexPath.row].name
        cell.contentView.backgroundColor = .clear
        cell.clipsToBounds = true
        cell.contentView.cornerRadius = 12
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if UserSettings.coins < 2{
            let vc = HDStoryboard.shared.child2(withIdentifier: "coinVC")
            self.present(vc!, animated: true, completion: nil)
            return
        }
        UserSettings.coins -= 2
        NotificationCenter.default.post(name: Notification.Name("coinChange"), object: nil)
        
        let vc = HDStoryboard.shared.child(withIdentifier: "videoPlayerVC") as! VideoPlayerViewController
        
        vc.emote = shouldUseTemp ? self.tempEmotes[indexPath.row] : self.emotes[indexPath.row]
        self.present(vc, animated: true, completion: nil)
        
        return;
        
        guard let url = URL(string: emotes[indexPath.row].link) else {
          return //be safe
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            shouldUseTemp = false
            self.collection.reloadData()
            return
        }
        tempEmotes.removeAll()
        for item in emotes{
            if item.name.contains(searchBar.text!.lowercased()){
                tempEmotes.append(item)
            }
        }
        shouldUseTemp = true
        collection.reloadData()
    }
    
    
    
    
}




public enum Alignment {
    case justified
    case left
    case center
    case right
}

public class FlowLayout: UICollectionViewFlowLayout {
    
    typealias AlignType = (lastRow: Int, lastMargin: CGFloat)
    
    var align: Alignment = .right
    
    override public func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let collectionView = collectionView else { return nil }
        guard let attributes = super.layoutAttributesForElements(in: rect) else { return nil }
        
        let shifFrame: ((UICollectionViewLayoutAttributes) -> Void) = { [unowned self] layoutAttribute in
            if layoutAttribute.frame.origin.x + layoutAttribute.frame.size.width > collectionView.bounds.size.width {
                layoutAttribute.frame.size.width = collectionView.bounds.size.width - self.sectionInset.left - self.sectionInset.right
            }
        }
        
        var leftMargin = sectionInset.left
        var maxY: CGFloat = -1.0
        var alignData: [AlignType] = []
        
        attributes.forEach { layoutAttribute in
            switch align {
            case .left, .center, .right:
                if layoutAttribute.frame.origin.y >= maxY {
                    alignData.append((lastRow: layoutAttribute.indexPath.row, lastMargin: leftMargin - minimumInteritemSpacing))
                    leftMargin = sectionInset.left
                }
                
                shifFrame(layoutAttribute)
                
                layoutAttribute.frame.origin.x = leftMargin
                
                leftMargin += layoutAttribute.frame.width + minimumInteritemSpacing
                
                maxY = max(layoutAttribute.frame.maxY , maxY)
            case .justified:
                shifFrame(layoutAttribute)
            }
        }
        
        align(attributes: attributes, alignData: alignData, leftMargin: leftMargin - minimumInteritemSpacing)
        
        return attributes
    }
    
    private func align(attributes: [UICollectionViewLayoutAttributes], alignData: [AlignType], leftMargin: CGFloat) {
        guard let collectionView = collectionView else { return }
        
        switch align {
        case .left, .justified:
            break
        case .center:
            attributes.forEach { layoutAttribute in
                if let data = alignData.filter({ $0.lastRow > layoutAttribute.indexPath.row }).first {
                    layoutAttribute.frame.origin.x += ((collectionView.bounds.size.width - data.lastMargin - sectionInset.right) / 2)
                } else {
                    layoutAttribute.frame.origin.x += ((collectionView.bounds.size.width - leftMargin - sectionInset.right) / 2)
                }
            }
        case .right:
            attributes.forEach { layoutAttribute in
                if let data = alignData.filter({ $0.lastRow > layoutAttribute.indexPath.row }).first {
                    layoutAttribute.frame.origin.x += (collectionView.bounds.size.width - data.lastMargin - sectionInset.right)
                } else {
                    layoutAttribute.frame.origin.x += (collectionView.bounds.size.width - leftMargin - sectionInset.right)
                }
            }
        }
    }
}
