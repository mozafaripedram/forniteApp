//
//  InternetServices.swift
//  Among Us
//
//  Created by pedram on 11/25/20.
//

import Foundation
import Alamofire
import SwiftyJSON


class InternetServices {
    static var shared = InternetServices()
    
    
    func getVideos(cat : String? , isSeries : Bool = false ,_ completion : @escaping completionViedos){
        var link = cat == nil ? "\(VIDEO_URL)list" : "\(VIDEO_URL)list?\(cat!)"
        var videos = [Video]()
        print(link)
        AF.request(link, method: .get, parameters: nil).validate().responseJSON { (response) in
            switch response.result{
            case .success(let value):
                do {
                    let json = JSON(value)
                    print(json)
                    videos = try JSONDecoder().decode([Video].self, from: json.description.data(using: .utf8)!)
                    
                    completion(videos)
                }catch{
                    completion(videos)
                }
            case .failure(let error):
                print(error.localizedDescription)
                completion(videos)
            }
        }
    }
    
    
    func getSubSeries(name : String,_ completion : @escaping completionViedos){
        let link = "\(VIDEO_URL)list?cat=series&serieName=\(name)"
        var videos = [Video]()
        AF.request(link, method: .get, parameters: nil).validate().responseJSON { (response) in
            switch response.result{
            case .success(let value):
                do {
                    let json = JSON(value)
                    print(json)
                    videos = try JSONDecoder().decode([Video].self, from: json.description.data(using: .utf8)!)
                    
                    completion(videos)
                }catch{
                    completion(videos)
                }
            case .failure(let error):
                print(error.localizedDescription)
                completion(videos)
            }
        }
    }
    
    
    func getSeries(_ completion : @escaping completionSeries) {
        let link = "\(VIDEO_URL)list?seriesList=true"
        var series = [Series]()
        AF.request(link, method: .get, parameters: nil).validate().responseJSON { (response) in
            switch response.result{
            case .success(let value):
                do {
                    let json = JSON(value)
                    print(json)
                    for item in json.arrayValue{
                        let seri = Series(id: item["_id"].stringValue)
                        series.append(seri)
                    }
                    completion(series)
                }catch{
                    print(error.localizedDescription)
                    completion(series)
                }
            case .failure(let error):
                print(error.localizedDescription)
                completion(series)
            }
        }
    }
    
    
    func getWallpapers(skip : Int?,_ completion : @escaping completionWallpaper){
        let link = skip == nil ? "\(WALLPAPER_URL)list" : "\(WALLPAPER_URL)list?\(skip!)&30"
        print(link)
        var walls = [Wallpaper]()
        AF.request(link, method: .get, parameters: nil).validate().responseJSON { (response) in
            switch response.result{
            case .success(let value):
                do {
                    let json = JSON(value)
                    walls = try JSONDecoder().decode([Wallpaper].self, from: json.description.data(using: .utf8)!)
                    
                    completion(walls)
                }catch{
                    completion(walls)
                }
            case .failure(let error):
                print(error.localizedDescription)
                completion(walls)
            }
        }
    }
    
    
    func getStickers(skip : Int?,_ completion : @escaping completionWallpaper){
        let link = skip == nil ? "\(STICEKR_URL)list" : "\(STICEKR_URL)list?\(skip!)&30"
        var walls = [Wallpaper]()
        AF.request(link, method: .get, parameters: nil).validate().responseJSON { (response) in
            switch response.result{
            case .success(let value):
                do {
                    let json = JSON(value)
                    walls = try JSONDecoder().decode([Wallpaper].self, from: json.description.data(using: .utf8)!)
                    
                    completion(walls)
                }catch{
                    completion(walls)
                }
            case .failure(let error):
                print(error.localizedDescription)
                completion(walls)
            }
        }
    }
    
    func getVideoCategory(_ completion : @escaping completionStringArray){
        let link = "\(GET_VIDEO_CATEGORY)"
        var cats = [String]()
        AF.request(link, method: .get, parameters: nil).validate().responseJSON { (response) in
            switch response.result{
            case .success(let value):
                do {
                    let json = JSON(value)
                    let array = json.arrayValue
                    print(json)
                    print(array)
                    for item in array {
                        cats.append(item["_id"].stringValue)
                    }
                    
                    completion(cats)
                }catch{
                    completion(cats)
                }
            case .failure(let error):
                print(error.localizedDescription)
                completion(cats)
            }
        }
    }
    
    
    func getVideoByCategory(category : String ,  _ completion : @escaping completionViedos){
        let link = "\(GET_VIDEO_BY_CATEGORY)\(category.replacingOccurrences(of: " ", with: "%20"))"
        var vid = [Video]()
        print(link)
        AF.request(link, method: .get, parameters: nil).validate().responseJSON { (response) in
            switch response.result{
            case .success(let value):
                do {
                    let json = JSON(value)
                    print(json)
                    vid = try JSONDecoder().decode([Video].self, from: json.description.data(using: .utf8)!)
                    completion(vid)
                }catch{
                    completion(vid)
                }
            case .failure(let error):
                print(error.localizedDescription)
                completion(vid)
            }
        }
    }
    
    
    func getEmotes(_ completion : @escaping completionEmote){
        let link = "\(GET_EMOTE)"
        var emote = [Emote]()
        print(link)
        AF.request(link, method: .get, parameters: nil).validate().responseJSON { (response) in
            switch response.result{
            case .success(let value):
                do {
                    let json = JSON(value)
                    print(json)
                    emote = try JSONDecoder().decode([Emote].self, from: json.description.data(using: .utf8)!)
                    completion(emote)
                }catch{
                    completion(emote)
                }
            case .failure(let error):
                print(error.localizedDescription)
                completion(emote)
            }
        }
    }
    
    func searchEmote(search : String ,_ completion : @escaping completionEmote){
        let link = "\(EMOTE_SEARCH)\(search)"
        var emote = [Emote]()
        
        AF.request(link, method: .get, parameters: nil).validate().responseJSON { (response) in
            switch response.result{
            case .success(let value):
                do {
                    let json = JSON(value)
                    print(json)
                    emote = try JSONDecoder().decode([Emote].self, from: json.description.data(using: .utf8)!)
                    completion(emote)
                }catch{
                    completion(emote)
                }
            case .failure(let error):
                print(error.localizedDescription)
                completion(emote)
            }
        }
    }
    
    
    func getStat(name : String,isFriend : Bool = false , _ completion : @escaping completionInfo){
        let link = "https://fortnite-api.com/v1/stats/br/v2?name=\(name)"
        var info : Info?
        AF.request(link, method: .get, parameters: nil).validate().responseJSON { (response) in
            switch response.result{
            case .success(let value):
                do {
                    let json = JSON(value)
                    
                    let status = json["status"].intValue
                    print(status)
                    if status == 200 {
                        let m = json["data"]["stats"]["all"]["overall"]
                        print(m)
                        info = try JSONDecoder().decode(Info.self, from: m.description.data(using: .utf8)!)
                        if !isFriend{
                            UserSettings.userInfo = m.description
                        }
                        completion(status , info)
                    }else{
                        completion(404 , info)
                    }
                    
                }catch{
                    print(error.localizedDescription)
                    completion(404 , info)
                }
            case .failure(let error):
                print(error.localizedDescription)
                completion(404 , info)
            }
        }
    }
    
    
}




